<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            // profile
            $table->string('name')->comment('姓名');
            $table->string('code')->unique()->comment('會員編號');
            $table->date('birth')->nullable()->comment('生日');
            $table->boolean('gender')->nullable()->comment('1:男 2:女');
            $table->string('role')->nullable()->comment('會員身份');
            $table->string('address')->nullable()->comment('聯絡地址');
            $table->string('email')->nullable()->comment('電子郵件信箱');
            $table->string('avatar')->nullable()->comment('頭像');
            $table->string('phone')->nullable()->comment('電話');
            $table->string('mobile')->nullable()->comment('手機');
            $table->string('facebook_name')->nullable()->comment('臉書名稱');
            $table->string('line_id')->nullable()->comment('Line ID');
            $table->string('identity')->nullable()->comment('身份');
            $table->text('education')->nullable()->comment('學歷');
            $table->text('experience')->nullable()->comment('經歷');
            $table->string('clinic_name')->nullable()->comment('現職診所');
            $table->string('clinic_city')->nullable()->comment('服務診所城市');
            $table->string('clinic_district')->nullable()->comment('服務診所區');
            $table->string('clinic_address')->nullable()->comment('服務診所地址');
            $table->boolean('show_education')->default(TRUE)->comment('是否顯示學歷');
            $table->boolean('show_experience')->default(TRUE)->comment('是否顯示經歷');
            $table->boolean('show_current_job')->default(TRUE)->comment('是否顯示現職資料');
            $table->string('certificate_number')->nullable()->comment('牙醫師證書編號');
            $table->text('payment_record')->nullable()->comment('繳費紀錄');
            $table->text('note')->nullable()->comment('備註');
            $table->boolean('is_paid')->default(0)->comment('是否繳交常年會費');
            $table->boolean('is_checked')->default(FALSE)->comment('是否檢核');

            // account
            $table->string('username')->comment('帳號');
            $table->string('password');
            $table->boolean('active')->default(0)->comment('是否啟用');
            $table->timestamp('active_until')->nullable()->comment('帳戶活動截止日');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
