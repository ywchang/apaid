<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->uuid,
        'role' => 'general',
        'username' => $faker->userName,
        'password' => $faker->password,
        'email' => [$faker->safeEmail],
        'clinic_city' => $faker->randomElement(config('apaid.city')),
        'clinic_district' => $faker->citySuffix,
        'clinic_address' => $faker->address
    ];
});
