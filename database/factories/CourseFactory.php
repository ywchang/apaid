<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'summary' => $faker->realText(),
        'start_at' => $faker->dateTime(),
        'end_at' => now()->addDays(3),
    ];
});
