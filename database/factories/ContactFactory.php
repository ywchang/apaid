<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'type' => 1,
        'name' => $faker->name,
        'gender' => 1,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'reply_method' => 1,
        'content' => $faker->paragraph,
        'is_checked' => true,
    ];
});
