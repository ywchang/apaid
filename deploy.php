<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'apaid');

// Project repository
set('repository', 'git@bitbucket.org:ywchang/apaid.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts
set('default_stage', '');

host('apaid.org')
    ->hostname('206.189.39.158')
    ->user('deployer')
    ->stage('production')
    ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'master')
    ->set('deploy_path', '/data/apaid');

// Tasks
task('build', function () {
    run('cd {{release_path}} && npm install');
    run('cd {{release_path}} && npm run production');
});

task('php-fpm:reload', function () {
    run("sudo service php7.4-fpm reload");
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
after('deploy:writable', 'build');
before('deploy:symlink', 'artisan:migrate');
after('deploy:symlink', 'artisan:optimize');
before('success', 'php-fpm:reload');

