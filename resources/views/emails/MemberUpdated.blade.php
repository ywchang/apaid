@component('mail::message')
# 會員資料更新通知

會員 **{{ $member->name }}** 的資料已經更新。

如有需要，請下載最新的會員清冊。

@component('mail::button', ['url' => route('admin.members.export')])
下載清冊
@endcomponent

謝謝,<br>
{{ config('app.name') }}
@endcomponent
