@component('mail::message')
# 聯絡我們表單

姓名：{{ $contact->name }}<br>
性別：{{ config('apaid.gender')[$contact->gender] ?? '未提供' }}<br>
希望聯繫方式：{{ config('apaid.contact.reply_method')[$contact->reply_method] }}<br>
電子郵件：{{ $contact->email }}
電話：{{ $contact->phone }}<br>
問題類型：{{ $contact->types->name }}<br>
問題內容：{{ $contact->content }}<br>

謝謝,<br>
{{ config('app.name') }}
@endcomponent
