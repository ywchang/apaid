<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>台灣亞洲植牙醫學會 | 管理後台</title>

    <link rel="stylesheet" href="{{ asset('vendors/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset("vendors/images/favicon.png") }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @yield('style')
</head>
<body>
<div id="app" class="container-scroller">
@include('admin.components.navbar', ['notification' => false, 'message' => false])
    <div class="container-fluid page-body-wrapper">
        @include('admin.components.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('admin.components.footer')
        </div>
    </div>
</div>
<script src="{{ asset('js/admin.js') }}"></script>
@yield('script')
</body>
</html>
