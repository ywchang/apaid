<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>台灣亞洲植牙醫學會 | APAID</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/materialdesignicons.min.css') }}">
    <style>
        body {
            background-color: #F7FAFB;
        }
    </style>
    @yield('style')
    @if(app()->isProduction())
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.gtag.id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config('services.gtag.id') }}');
    </script>
    @endif
</head>
<body>
<header id="nav">
    <top-navbar @auth :user="{{ auth()->user() }}" @endauth></top-navbar>
    @auth
        <form id="logout-form" action="{{ route('course.logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    @endauth
</header>
<div id="app">
    @yield('content')
</div>
<footer class="footer py-10 bg-gray-900 text-white">
    <div class="text-center mx-auto w-11/12 py-2 md:flex md:justify-center md:space-x-8">
        <div><i class="mdi mdi-earth menu-icon"></i> 台灣：<address class="inline">台北市士林區中正路120號B1</address></div>
        <div><i class="mdi mdi-phone-classic menu-icon"></i> 電話：<a href="tel:0228342816">(02)2834-2816 #225</a></div>
        <div><i class="mdi mdi-fax menu-icon"></i> 傳真：(02)2834-0745</div>
    </div>
    <div class="text-center mx-auto py-2 w-11/12">Copyright © 2020 Asia Pacific Academy of Implant Dentistry. All rights reserved.</div>
</footer>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    new Vue({ 'el': '#nav' })
</script>
@yield('script')
</body>
</html>
