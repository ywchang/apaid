@extends('course.profile-layout')

@section('main')
    <section class="pt-8">
        <div class="text-2xl md:text-3xl mb-5 font-bold">繳費記錄</div>
        <table class="table-fixed w-full">
            <tr class="bg-gray-300 text-gray-700">
                <th class="p-2">日期</th>
                <th class="p-2">項目名稱</th>
                <th class="p-2">金額</th>
                <th class="p-2">繳費方式</th>
            </tr>
            @forelse($member->payment_record as $payment)
                <tr class="text-center text-gray-700 hover:bg-gray-300 hover:opacity-75">
                    <td class="p-2">{{ $payment['date'] }}</td>
                    <td class="p-2">{{ $payment['title'] }}</td>
                    <td class="p-2">{{ number_format($payment['amount']) }}</td>
                    <td class="p-2">{{ $payment['method'] }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">沒有繳費記錄</td>
                </tr>
            @endforelse
        </table>
    </section>
@endsection
