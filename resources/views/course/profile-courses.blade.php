@extends('course.profile-layout')

@section('main')
    <section class="pt-8">
        <div class="text-2xl md:text-3xl mb-5 font-bold">我修的課</div>
        <div class="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-4">
            @foreach($courses as $course)
                @component('course.components.course-card', ['course' => $course])
                @endcomponent
            @endforeach
        </div>
    </section>
    @if ($courses->hasPages())
    <div class="pt-8">
        {{ $courses->links() }}
    </div>
    @endif
@endsection
