@extends('layouts.html')

@section('style')
    <style>
        .container {
            min-height: calc(100vh - 140px - 75px);
        }
    </style>
@endsection

@section('content')
    <div class="container w-11/12 mx-auto py-8 md:pt-20 md:pb-10">
        <div v-if="isLoaded">
            <h2 class="text-3xl mb-3">@{{ lesson.title }}</h2>
            <div class="flex flex-col md:flex-row h-32 md:h-auto overflow-scroll md:flex-wrap mb-5">
                <div v-for="material of lesson.materials"
                     @click.prevent="setMaterial(material)"
                     class="lg:w-1/6 md:w-1/4 break-all content-center cursor-pointer hover:bg-gray-300 px-3 py-2 rounded">
                    <span class="flex items-center">
                        <i class="text-2xl md:text-5xl mdi mdi-file-pdf text-red-700" v-if="material.type === 'pdf'"></i>
                        <i class="text-2xl md:text-5xl mdi mdi-youtube text-red-700" v-else-if="material.type === 'video'"></i>
                        <i class="text-2xl md:text-5xl mdi mdi-image-outline text-red-700" v-else-if="material.type === 'image'" ></i> @{{ material.name }}
                    </span>
                </div>
            </div>
            <div class="py-5 flex" v-if="lesson.materials.length !== 0">
                <div class="flex-1" v-if="current.type === 'video'">
                    <video-player v-on:complete="completeMaterial" :material="current"></video-player>
                </div>
                <div class="flex-1" v-show="current.type === 'image'">
                    <img v-if="current.type === 'image'" :src="current.url" alt="" oncontextmenu="return false">
                </div>
                <div class="flex-1" v-show="current.type === 'pdf'" oncontextmenu="return false">
                    <iframe v-if="current.type === 'pdf'" :src="'/pdfjs/web/viewer.html?file=' + current.url" oncontextmenu="return false" style="overflow: auto; width: 100%; min-height: 90vh;"></iframe>
                </div>
            </div>
            <div class="mb-3 flex">
                <span class="text-green-500" style="min-width: 5rem;">教師名稱</span>
                <div>@{{ lesson.teacher }}</div>
            </div>
            <div class="mb-3 flex">
                <span class="text-green-500" style="min-width: 5rem;">教學大綱</span>
                <div class="tracking-widest" v-html="lesson.syllabus"></div>
            </div>
        </div>
        <div class="my-10">
            <a class="cursor-pointer text-green-500 border border-green-500 hover:bg-green-500 hover:text-white px-5 py-2 rounded-full" href="{{ route('course.courses.show', $course->id) }}"> « 回到課程頁 </a>
        </div>
    </div>
@endsection

@section('script')
    <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
    <script>
        vueApp = {
            el: '#app',
            data: {
                current: {},
                course: {},
                lesson: {
                    materials: []
                },
                isLoaded: false
            },
            methods: {
                setMaterial(target) {
                    this.current = target;
                    if (target.type !== 'video')
                        this.completeMaterial()
                },
                completeMaterial() {
                    axios.post(`/courses/${this.course.id}/material/${this.current.id}/complete`)
                }
            },
            mounted() {
                let vm = this;
                axios.get('/api' + window.location.pathname)
                    .then(response => {
                        vm.lesson = response.data.lesson
                        vm.course = response.data.course
                        this.setMaterial(vm.lesson.materials[0]);
                    })
                    .finally(() => vm.isLoaded = true);
            }
        };
        const app = new Vue(vueApp);
    </script>
@endsection
