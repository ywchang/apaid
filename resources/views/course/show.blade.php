@extends('layouts.html')

@section('style')
    <style>

    </style>
@endsection

@section('content')
    <div class="container mx-auto w-11/12 py-8">
        <template v-if="isLoaded">
            <img :src="'/storage/' + course.banner" class="shadow-xl w-full mb-8" alt="課程橫幅" v-if="course.banner != null">
            <img :src="'//place-hold.it/1200x400/cbd5e0/fff/4a5568?text=Banner&fontsize=24'" class="shadow-xl w-full mb-8" alt="課程橫幅" v-else>
            <h2 class="course-title text-4xl mb-4">@{{ course.title }}</h2>
            <div class="course-information mb-12">
                <div class="mb-3 flex">
                    <span class="text-green-500" style="min-width: 5rem;">教師名稱</span>
                    <div>@{{ course.teacher }}</div>
                </div>
                <div class="mb-3 flex">
                    <span class="text-green-500" style="min-width: 5rem;">上課時間</span>
                    <div>@{{ course.start_at }} ~ @{{ course.end_at }}</div>
                </div>
                <div class="mb-3 flex">
                    <span class="text-green-500" style="min-width: 5rem;">上課地點</span>
                    <div>@{{ course.address }}</div>
                </div>
                <div class="mb-3 flex">
                    <span class="text-green-500" style="min-width: 5rem;">報名費用</span>
                    <div>@{{ course.price }}</div>
                </div>
                <div>
                    <div class="text-green-500 mb-3" style="min-width: 5rem;">課程簡介</div>
                    <div class="tracking-widest" v-html="course.summary"></div>
                </div>
            </div>
            <section>
                <div class="text-green-500 mb-3">課程教學</div>
                <table class="table-fixed w-full">
                    <thead>
                    <tr>
                        <th class="px-4 py-2">主題</th>
                        <th class="px-4 py-2">講師</th>
                        <th class="px-4 py-2">上課時間</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr v-for="lesson of course.lessons">
                        <td class="border px-4 py-2">
                            <a class="text-green-500" :href="`/courses/${course.id}/lessons/${lesson.id}`" v-if="lesson.canRead">@{{ lesson.title }}</a>
                            <span v-else>@{{ lesson.title }}</span>
                        </td>
                        <td class="border px-4 py-2">@{{ lesson.teacher }}</td>
                        <td class="border px-4 py-2">@{{ lesson.start_at }} ~ @{{ lesson.end_at }}</td>
                    </tr>
                    </tbody>
                </table>
            </section>
        </template>
        <div v-else>
            資料載入中 ...
        </div>
    </div>
@endsection

@section('script')
    <script>
        new Vue({
            el: '#app',
            data: {
                course: {},
                isLoaded: false
            },
            mounted() {
                let vm = this;
                axios.get('/api' + window.location.pathname)
                    .then(response => vm.course = response.data.course)
                    .finally(() => vm.isLoaded = true);
            }
        });
    </script>
@endsection
