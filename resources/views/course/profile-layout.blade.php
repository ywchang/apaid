@extends('layouts.html')

@section('style')
    <style>
        .container {
            min-height: calc(100vh - 160px - 66px - 382px);
        }
        .view-profile {
            background-color: #F3F3F3;
            min-height: 382px;
        }
        .tabs {
            top: -41px;
        }

        .tabs a.active {
            background: #F7FAFB;
        }
    </style>
@endsection

@section('content')
    <div class="min-w-full pt-8 pb-24 md:pb-8 view-profile flex flex-col md:flex-row justify-center items-center">
        <div class="avatar rounded-full shadow-sm mb-4 md:mb-0">
                <img class="p-1 border border-gray-400 rounded-md"
                     width="200" alt="{{ $member->name }}的照片"
                     src="{{ $member->avatar ? asset("storage/{$member->avatar}") : asset("images/avatar-elite.jpg") }}"
                >
        </div>
        <div class="flex flex-col md:pl-10">
            <div class="font-bold"><span class="text-3xl">{{ $member->name }}</span><span class="ml-1 text-xl text-gray-700">{{ $member->identity }}</span> </div>
            <div class="text-green-500 mb-4 text-opacity-75">{{ $member->role() }}</div>
            <a class="rounded-full px-10 py-3 bg-green-500 text-white bg-opacity-75 hover:bg-opacity-100" href="{{ route('course.profile.edit') }}">編輯個人檔案</a>
        </div>
    </div>
    <div class="container main mx-auto w-11/12 py-10 relative">
        <div class="absolute tabs">
            <ul class="list-reset flex">
                <li class="-mb-px mr-1">
                    <a class="inline-block py-2 px-4 font-semibold @if('profile' === app('router')->current()->uri) border-l border-t border-r rounded-t active @endif"
                       href="{{ route('course.profile.show') }}">我修的課</a>
                </li>
                <li class="mr-1">
                    <a class="inline-block py-2 px-4 font-semibold @if('profile/records' === app('router')->current()->uri) border-l border-t border-r rounded-t active @endif" href="{{ route('course.profile.records') }}">繳費記錄</a>
                </li>
            </ul>
        </div>
        @if (!$member->is_paid)
            <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                <p class="font-bold">繳費通知</p>
                <p>本年度{{ now()->format("Y") }}常年會費尚未繳交，請儘速完成會費繳納</p>
            </div>
        @endif
        @yield('main')
    </div>
@endsection
