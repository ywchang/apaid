@extends('layouts.html')


@section('style')
    <style>
        .view-profile {
            background-color: #F3F3F3;
            min-height: 382px;
        }
        .form-select {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='%23a0aec0'%3e%3cpath d='M15.3 9.3a1 1 0 0 1 1.4 1.4l-4 4a1 1 0 0 1-1.4 0l-4-4a1 1 0 0 1 1.4-1.4l3.3 3.29 3.3-3.3z'/%3e%3c/svg%3e");
            background-repeat: no-repeat;
            background-position: right 0.5rem center;
            background-size: 1.5em 1.5em;
        }
    </style>
@endsection

@section('content')
    <div class="min-w-full py-8 view-profile flex flex-col md:flex-row justify-center items-center">
        <div class="relative rounded-full mb-4 md:mb-0 cursor-pointer" onclick="document.getElementById('avatar-input').click()">
            <img class="p-1 border border-gray-400 rounded-md"
                 width="200" alt="{{ $member->name }}的照片"
                 src="{{ $member->avatar ? asset("storage/{$member->avatar}") : asset("images/avatar-elite.jpg") }}"
            >
            <div class="w-full h-full absolute leading-none flex flex-col justify-center space-y-2 top-0 left-0 opacity-0 hover:opacity-100 bg-opacity-0 hover:bg-opacity-50 bg-black rounded-md">
                <div class="w-8 h-8 mx-auto bg-white rounded-full flex items-center justify-center shadow">
                    <img src="{{ asset('images/pencil.png') }}" alt="edit">
                </div>
                <p class="text-gray-300 text-xs italic text-center">建議尺寸: 200 x 250</p>
            </div>
        </div>
        <div class="flex flex-col md:pl-10">
            <div class="font-bold"><span class="text-3xl">{{ $member->name }}</span><span class="ml-1 text-xl text-gray-700">{{ $member->identity }}</span> </div>
            <div class="text-green-500 mb-4 text-opacity-75">{{ $member->role() }}</div>
        </div>
        <input type="file" id="avatar-input" @change="handleUpload" class="hidden" accept="image/*">
    </div>
    <div class="container mx-auto w-11/12 py-8 space-y-8">
        <div class="text-2xl md:text-3xl font-bold">編輯個人資料</div>
        <form class="bg-white p-10 shadow-md border rounded-md" action="{{ route('course.profile.update') }}" method="POST">
            @method('PUT')
            @csrf
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="name">
                        姓名 <span class="text-red-600">*</span>
                    </label>
                    <input id="name" type="text" name="name" value="{{ old('name', $member->name) }}"
                           class="@error('name') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('name') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="email">
                        電子郵件 <span class="text-red-600">*</span>
                    </label>
                    <input id="email" name="email" type="text" v-model="emailText"
                           class="@error('email.*') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <input type="hidden" v-for="email of emails" name="email[]" :value="email">
                    <p class="text-gray-600 text-xs italic">多組 Email 請使用逗號分隔。 範例: email1@apaid.asia,
                        email2@apaid.asia</p>
                    @error('email.*') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="password">
                        密碼
                    </label>
                    <input id="password" name="password" :type="passwordType" placeholder="●●●●●●●●●●"
                           class="@error('password') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <label class="md:w-2/3 block text-gray-700 font-bold mb-3">
                        <input class="mr-2 leading-tight" type="checkbox" v-model="showPassword">
                        <span class="text-xs">顯示密碼</span>
                    </label>
                    <span class="text-gray-600 text-xs italic">如不修改密碼，請略過此欄位</span>
                    @error('password') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror

                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="birth">
                        生日<span class="text-red-600"> *</span>
                    </label>
                    <input id="birth" type="date" name="birth"
                           value="{{ old('birth', $member->birth ? $member->birth->format("Y-m-d") : null) }}"
                           class="@error('birth') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    <p class="text-gray-600 text-xs italic">日期格式: {{ now()->format("Y-m-d") }}</p>
                    @error('birth') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="gender">
                        性別<span class="text-red-600"> *</span>
                    </label>
                    <select id="gender" name="gender"
                            class="form-select @error('gender') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        <option value="1" @if($member->gender == 1) selected @endif>男</option>
                        <option value="2" @if($member->gender == 2) selected @endif>女</option>
                    </select>
                    @error('birth') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="phone">
                        聯絡電話 <span class="text-red-600">*</span>
                    </label>
                    <input id="phone" type="text" name="phone"
                           value="{{ old('phone', $member->phone) }}"
                           class="@error('phone') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('phone') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror

                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="mobile">
                        手機 <span class="text-red-600">*</span>
                    </label>
                    <input id="mobile" type="text" name="mobile"
                           value="{{ old('mobile', $member->mobile) }}"
                           class="@error('mobile') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('mobile') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="facebook-name">
                        Facebook 名稱
                    </label>
                    <input id="facebook-name" type="text" name="facebook_name"
                           value="{{ old('facebook_name', $member->facebook_name) }}"
                           class="@error('facebook_name') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('facebook_name') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror

                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="line-id">
                        Line ID
                    </label>
                    <input id="line-id" type="text" name="line_id"
                           value="{{ old('line_id', $member->line_id) }}"
                           class="@error('line_id') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('line_id') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-3" for="experience">
                        學 / 經歷 <span class="text-red-500">*</span>
                    </label>
                    <textarea id="experience" name="experience" class="@error('experience') border-red-500 @else border-gray-200 focus:border-gray-500 @endif no-resize appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white h-48 resize-none" required="">{{ old('experience', $member->experience) }}</textarea>
                    @error('experience') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                    <label class="md:w-2/3 block text-gray-700 font-bold mb-3">
                        <input class="mr-2 leading-tight" name="show_experience" @if($member->show_experience) checked @endif type="checkbox">
                        <span class="text-xs">在頁面上顯示學 / 經歷</span>
                    </label>
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="clinic-name">
                        診所名稱 / 職稱<span class="text-red-600"> *</span>
                    </label>
                    <input id="clinic-name" type="text" name="clinic_name"
                           value="{{ old('clinic_name', $member->clinic_name) }}"
                           class="@error('clinic_name') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('clinic_name') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="certificate-number">牙醫師證書編號</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 pl-4 flex items-center pointer-events-none">
                            <span class="text-gray-700 sm:text-sm sm:leading-5">牙字第</span>
                        </div>
                        <input id="certificate-number" type="text" name="certificate_number" placeholder="000000"
                               value="{{ old('certificate_number', $member->certificate_number) }}"
                               class="@error('certificate_number') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 pl-16 mb-3 leading-tight focus:outline-none focus:bg-white"
                        >
                        <div class="absolute inset-y-0 right-0 pr-4 flex items-center pointer-events-none">
                            <span class="text-gray-700 sm:text-sm sm:leading-5">號</span>
                        </div>
                    </div>
                    @error('certificate_number') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full md:w-1/4 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="clinic-city">
                        服務城市 <span class="text-red-600"> *</span>
                    </label>
                    <select id="clinic-city" name="clinic_city"
                            class="form-select @error('clinic_city') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        @foreach(config('apaid.city') as $city)
                            <option @if(old('clinic_city', $member->clinic_city) == $city) selected @endif>{{ $city }}</option>
                        @endforeach
                    </select>
                    @error('clinic_city') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
                <div class="w-full md:w-1/4 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="clinic-district">
                        服務地區 <span class="text-red-600"> *</span>
                    </label>
                    <input id="clinic-district" type="text" name="clinic_district" placeholder="鄉/鎮/市/區"
                           value="{{ old('clinic_district', $member->clinic_district) }}"
                           class="@error('clinic_district') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('clinic_district') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
                <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="clinic-address">
                        診所地址 <span class="text-red-600"> *</span>
                    </label>
                    <input id="clinic-address" type="text" name="clinic_address" placeholder="詳細地址"
                           value="{{ old('clinic_address', $member->clinic_address) }}"
                           class="@error('clinic_address') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    >
                    @error('clinic_address') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
                <div class="w-full px-3">
                    <label class="md:w-2/3 block text-gray-700 font-bold mb-3">
                        <input class="mr-2 leading-tight" @if($member->show_current_job) checked @endif type="checkbox">
                        <span class="text-xs">在頁面上顯示現任職務</span>
                    </label>
                </div>
            </div>

            <div class="form-group flex justify-end">
                <a class="text-gray-500 px-6 py-3" href="{{ route('course.profile.show') }}">取消</a>
                <button
                    class="bg-green-500 bg-opacity-75 rounded-full text-white px-10 py-3 hover:bg-opacity-100 hover:text-green-500d">
                    儲存個人檔案
                </button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        vue = new Vue({
            el: '#app',
            data: {
                showPassword: false,
                emailText: "{{ implode(',', old('email', $member->email)) }}",
            },
            methods: {
                handleUpload(event) {
                    const vm = this;
                    let file = event.target.files[0];

                    const form = new FormData;
                    form.append('file', file);

                    axios.post("{{ route('course.profile.avatar') }}", form)
                        .then(xhr => document.getElementById('avatar-input').setAttribute('src', '/storage/' + xhr.data.filePath))
                        .finally(() => event.target.value = '');
                }
            },
            computed: {
                emails() {
                    return this.emailText.split(',')
                },
                passwordType() {
                    return this.showPassword ? 'text' : 'password'
                }
            }
        })
    </script>
@endsection
