@extends('layouts.html')

@section('style')
    <style>
        .container {
            min-height: calc(100vh - 140px - 75px);
        }
        .is-invalid input {

        }
    </style>
@endsection
@section('content')
    <div class="container mx-auto py-20 flex items-center justify-center w-11/12">
        <div class="w-full max-w-xs">
            <div>
                <h2 class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
                    學員登入
                </h2>
            </div>
            <form class="mt-8" action="{{ route('course.login') }}" method="POST">
                @csrf
                <div class="rounded-md shadow-sm">
                    <div>
                        <input aria-label="Email address" type="text" required
                               name="username"
                               class="@error('username') border-red-500 @enderror appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                               value="{{ old('username') }}"
                               placeholder="帳號">
                    </div>
                    <div class="-mt-px">
                        <input aria-label="Password" name="password" type="password" required
                               class="@error('username') border-red-500 @enderror appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 sm:text-sm sm:leading-5"
                               placeholder="密碼">
                    </div>
                </div>
                @error('username')
                <small class="text-red-500" role="alert">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror

                <div class="mt-6 flex items-center justify-between">
                    <div class="flex items-center">
                        <input id="remember" type="checkbox"
                               name="remember"
                               class="form-checkbox h-4 w-4 transition duration-150 ease-in-out"
                                {{ old('remember') ? 'checked' : '' }}
                        >
                        <label for="remember" class="ml-2 block text-sm leading-5 text-gray-900">
                            記住我
                        </label>
                    </div>

                    <div class="text-sm leading-5">
                        <a href="#"
                           class="font-medium text-green-600 hover:text-green-500 focus:outline-none focus:underline transition ease-in-out duration-150">
                            忘記密碼?
                        </a>
                    </div>
                </div>

                <div class="mt-6">
                    <button type="submit"
                            class="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:border-green-600 focus:shadow-outline-green active:bg-green-700 transition duration-150 ease-in-out">
                        <span class="absolute left-0 inset-y-0 flex items-center pl-3">
                            <svg class="h-5 w-5 text-green-500 group-hover:text-green-400 transition ease-in-out duration-150"
                                 fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd"
                                      d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                                      clip-rule="evenodd"/>
                            </svg>
                        </span>
                        登入
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
