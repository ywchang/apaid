@extends('layouts.html')

@section('content')
    <div class="container w-11/12 mx-auto py-8 md:pt-20 md:pb-10">
        <section>
            <div class="text-2xl mb-5 font-bold">所有課程</div>
            <div class="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-4">
                @foreach($courses as $course)
                    @component('course.components.course-card', ['course' => $course])@endcomponent
                @endforeach
            </div>
        </section>
        <div class="pt-8">
            {{ $courses->links() }}
        </div>
    </div>
@endsection
