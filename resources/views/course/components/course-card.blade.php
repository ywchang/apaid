<div class="ms:max-w-sm rounded overflow-hidden shadow-lg">
    <div class="w-full bg-cover bg-gray-300" style='
        height: 170px;
        background-image: @if($course->banner) url({{ asset("storage/$course->banner") }}) @else url("//place-hold.it/1200x400/cbd5e0/fff/4a5568?text={{ $course->title }}&fontsize=56") @endif
        '>
    </div>
    <div class="text-xl mb-2 px-6 py-4">
        <div class="course-title text-xl mb-2 h-16">
            <a href="{{ route('course.courses.show', $course->id) }}">{{ $course->title }}</a>
        </div>
        <div class="text-sm mb-2 flex space-x-5">
            <span class="text-green-500">名稱</span>
            <span>{{ $course->teacher }}</span>
        </div>
        <div class="text-sm mb-8 flex space-x-5">
            <span class="text-green-500">上課期間</span>
            <span class="">{{ $course->start_at->format("Y-m-d") }} ~ {{ $course->end_at->format("Y-m-d") }}</span>
        </div>
        <div class="text-sm text-white">
            @foreach($course->tags as $tag)
                <span class="bg-green-500 hover:bg-green-700 cursor-pointer py-1 px-4 rounded-full">{{ $tag->name }}</span>
            @endforeach
        </div>
    </div>
</div>
