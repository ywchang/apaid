@extends('layouts.html')

@section('style')
    <style>

        .min-w-200 {
            min-width: 200px;
        }
    </style>
@endsection

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li class="flex items-center">
                    <a href="#">植牙專科醫師</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li>
                    <a href="#" class="text-gray-500" aria-current="page">醫師查詢</a>
                </li>
            </ol>
        </nav>
        <div class="text-3xl mb-5 font-bold text-center">醫師查詢</div>

        <div class="md:flex md:space-x-4 mb-8 space-y-4 md:space-y-0">
            <section class="md:w-1/4 w-full space-y-4">
                <div class="border p-4 rounded shadow bg-white">
                    <div class="font-bold text-xl mb-2">查詢姓名</div>
                    <form action="{{ route('official.doctors.search') }}">
                        <input class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white border-gray-200 focus:border-gray-500 " type="text" name="name"
                               value="{{ request('name') }}">
                    </form>
                </div>
                <div class="border p-4 rounded shadow bg-white">
                    <div class="font-bold text-xl mb-2">縣市</div>
                    <div>
                        <a class="text-green-500 hover:text-green-700" href="{{ route('official.doctors.search') }}">
                            全縣市 ({{ $cities->map->sum()->sum() }})</a>
                    </div>
                    @foreach($cities as $city => $areas)
                        <div>
                            <a class="text-green-500 hover:text-green-700" href="?city={{ $city }}">
                                {{ $city ?: '未分類' }}({{ $areas->sum() }})
                            </a>
                        </div>
                        @if (request('city') && $city == request('city'))
                            <div class="ml-4">
                            @foreach($areas as $area => $count)
                                <div>
                                    <a class="text-green-500 hover:text-green-700"
                                       href="?city={{ request('city') }}&area={{ $area }}">
                                        {{ $area }}({{ $count }})
                                    </a>
                                </div>
                            @endforeach
                            </div>
                        @endif
                    @endforeach
                </div>
            </section>
            <section class="md:w-3/4 w-full space-y-3">
                @foreach($members as $key => $member)
                    <div class="border rounded bg-white md:flex p-4 shadow md:space-x-4">
                        <div class="py-2 md:py-0 min-w-200">
                            <img class="rounded mx-auto shadow-lg" alt="{{ $member->name }}的照片"
                                 width="200" height="250"
                                 src="{{ $member->avatar ? asset("storage/{$member->avatar}") : asset("images/avatar-elite.jpg") }}"
                            >
                        </div>
                        <div class="space-y-2 w-full">
                            <div class="font-bold">
                                <span class="text-2xl">{{ $member->name }}</span>
                                <span class="text-sm text-gray-600">醫師</span></div>
                            <div>
                                <div class="text-green-500 text-lg">{{ $member->clinic_name }}</div>
                                <div class="text-sm">
                                    {{ $member->clinic_city }}{{ $member->clinic_district }} {{ $member->clinic_address }}
                                </div>
                            </div>
                            <div class="md:flex md:space-x-2 space-y-2 md:space-y-0">
                                @if ($member->show_experience)
                                    <div class="md:flex-1">
                                        <div class="text-green-500 text-lg">學 / 經歷</div>
                                        <p class="text-sm">{!! nl2br($member->experience) !!}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
        <div>
            <div class="hidden md:block">
                {{ $members->links() }}
            </div>
            <div class="md:hidden">
                {{ $members->links('vendor.pagination.simple-bootstrap-4')}}
            </div>
        </div>
    </div>
@endsection
