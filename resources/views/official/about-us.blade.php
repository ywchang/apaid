@extends('layouts.html')

@section('style')
    <style>
        section ol, section ul {
            padding-left: 2rem;
        }

        section ol > li {
            list-style: decimal;
        }

        section ul > li {
            list-style: circle;
        }

        .content {
            line-height: 2;
            letter-spacing: .2em;
        }
    </style>
@endsection

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li class="flex items-center">
                    <a href="#">關於我們</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li>
                    <a href="#" class="text-gray-500" aria-current="page">{{ $title }}</a>
                </li>
            </ol>
        </nav>
        <section class="pb-10">
            <div class="text-3xl mb-10 font-bold text-center">{{ $title }}</div>
            <div class="content">{!! $post->content !!}</div>
        </section>
    </div>
@endsection
