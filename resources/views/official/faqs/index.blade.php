@extends('layouts.html')

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path
                            d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                    </svg>
                </li>
                <li>
                    <a class="text-gray-500" aria-current="page">常見問題集</a>
                </li>
            </ol>
        </nav>
        <section>
            <div class="text-3xl mb-5 font-bold text-center">常見問題集</div>
            <div class="shadow">
                <article v-for="(item, index) of list" class="border-b">
                    <div class="border-l-2 border-transparent" :class="{ 'border-green-500' : isOpen === index, 'bg-white' : isOpen === index }">
                        <header class="flex justify-between items-center p-5 pl-8 pr-8 cursor-pointer select-none" @click="isOpen = index">
                            <span class="text-grey-darkest font-thin text-xl" :class="isOpen === index ? 'text-green-500' : 'text-grey-darkest'">
                                @{{ item.question }}
                            </span>
                            <div class="rounded-full border w-7 h-7 flex items-center justify-center" :class="isOpen === index ? 'border-green-500 bg-green-500' : 'border-grey-500'">
                                <!-- icon by feathericons.com -->
                                <svg v-if="isOpen !== index" aria-hidden="true" class="" data-reactid="266" fill="none" height="24"
                                     stroke="#606F7B" stroke-linecap="round" stroke-linejoin="round"
                                     stroke-width="2"
                                     viewbox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="6 9 12 15 18 9">
                                    </polyline>
                                </svg>
                                <svg v-else aria-hidden="true" data-reactid="281" fill="none" height="24" stroke="white"
                                     stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                     viewbox="0 0 24 24"
                                     width="24" xmlns="http://www.w3.org/2000/svg">
                                    <polyline points="18 15 12 9 6 15">
                                    </polyline>
                                </svg>
                            </div>
                        </header>
                        <div v-if="isOpen === index">
                            <div class="pl-8 pr-8 pb-5 text-grey-darkest">
                                <p class="pl-4">
                                    @{{ item.answer }}
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <div class="pt-8">
            {{ $faqs->links() }}
        </div>
    </div>
@endsection


@section('script')
    <script>
        app = new Vue({
            el : '#app',
            data: {
                isOpen : -1,
                list : []
            },
            mounted() {
                this.list = @json($faqs->items())
            }
        })
    </script>
@endsection
