<section>
    <div class="font-bold py-4">文件下載：</div>
    @foreach($post->attachments as $attachment)
        <div class="py-2 mb-4">
            <a class="sm:text-sm md:inline flex md:text-lg text-gray-100 bg-green-500 hover:bg-green-700 rounded-full px-10 py-3" target="_blank" href="{{ url('storage/'. $attachment->path) }}">
                <i class="mdi mdi-progress-download pr-2"></i><span>{{ $attachment->name }}</span>
            </a>
        </div>
    @endforeach
</section>
