@extends('layouts.html')

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path
                            d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                    </svg>
                </li>
                <li>
                    <a class="text-gray-500" aria-current="page">學術活動</a>
                </li>
            </ol>
        </nav>
        <section class="mb-8">
            <div class="text-3xl mb-5 font-bold text-center">學術活動</div>
            <div class="md:flex md:space-x-4 mb-8 space-y-4 md:space-y-0">
                <section class="md:w-1/5 w-full space-y-4">
                    <div class="border p-4 rounded shadow bg-white">
                        <div class="font-bold text-xl mb-2">歷年學術活動</div>
                        <div class="pt-2">
                            @foreach($years as $year)
                                <div class="mb-1">
                                    <a href="?year={{ $year }}" class="text-green-500 hover:text-green-700">{{ $year }} 年活動</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                <section class="md:w-4/5 w-full space-y-3">
                    <table class="table-auto w-full">
                        <tr class="bg-gray-300">
                            <th class="p-2 text-gray-700" colspan="2">標題</th>
                            <th class="p-2 text-gray-700">刊登日期</th>
                        </tr>
                        @forelse($events as $key => $event)
                            <tr class="text-gray-700 hover:bg-gray-300 hover:opacity-75">
                                <td class="p-2 text-center">
                                    @if ($event->image)
                                        <img class="rounded mx-auto" src="/storage/{{ $event->image }}" width="130"  alt="{{ $event->title }} banner">
                                        @else
                                        <img class="rounded mx-auto" src="{{ asset('images/event_130x81.gif') }}" width="130"  alt="{{ $event->title }} banner">
                                    @endif
                                </td>
                                <td class="p-2 hover:text-green-500 tracking-wider"><a href="{{ route('official.events.show', $event->id) }}">{{ $event->title }}</a></td>
                                <td class="p-2 text-center tracking-widest">{{ $event->publish_at->format("Y-m-d") }}</td>
                            </tr>
                        @empty
                            <tr class="text-gray-700">
                                <td class="p-2" colspan="2">沒有活動紀錄</td>
                            </tr>
                        @endforelse
                    </table>
                </section>
            </div>
        </section>
        <div>
            <div class="hidden md:block">
                {{ $events->links() }}
            </div>
            <div class="md:hidden">
                {{ $events->links('vendor.pagination.simple-bootstrap-4')}}
            </div>
        </div>
    </div>
@endsection
