@extends('layouts.html')

@section('style')
    <style>
        .main {
            margin-bottom: 150px;
        }
    </style>
@endsection

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path
                            d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                    </svg>
                </li>
                <li class="flex items-center">
                    <a href="{{ route('official.news.index') }}">最新消息</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path
                            d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                    </svg>
                </li>
                <li>
                    <a class="text-gray-500" aria-current="page">最新消息內容</a>
                </li>
            </ol>
        </nav>
        <section class="main">
            <div class="text-3xl mb-5 font-bold text-center">{{ $news->title }}</div>
            <div class="flex mb-5 space-x-4">
                @if (count($news->tags))
                    <div
                        class="text-green-500 border border-green-500 rounded-full px-6 py-2">{{ $news->tags[0]->name }}</div>
                @endif
                <div class="py-2">{{ $news->publish_at->format("Y-m-d") }}</div>
            </div>
            <div class="overflow-x-scroll leading-8 tracking-widest">{!! $news->content !!}</div>
        </section>
        <div class="relative">
            <a class="flex items-center absolute hover:bg-green-500 pr-8 hover:text-gray-300 text-green-500 py-3 px-3 rounded-full bottom-0" href="{{ route('official.news.index') }}">
                <svg class="fill-current h-5 w-5 mx-3" x-description="Heroicon name: chevron-left"
                     viewBox="0 0 20 20"
                     fill="currentColor">
                    <path fill-rule="evenodd"
                          d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                          clip-rule="evenodd"></path>
                </svg>
                回最新消息列表
            </a>
        </div>
    </div>
@endsection
