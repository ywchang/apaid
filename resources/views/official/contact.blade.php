@extends('layouts.html')

@section('style')
    <style>
        .form-select {
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='%23a0aec0'%3e%3cpath d='M15.3 9.3a1 1 0 0 1 1.4 1.4l-4 4a1 1 0 0 1-1.4 0l-4-4a1 1 0 0 1 1.4-1.4l3.3 3.29 3.3-3.3z'/%3e%3c/svg%3e");
            background-repeat: no-repeat;
            background-position: right 0.5rem center;
            background-size: 1.5em 1.5em;
        }
    </style>
@endsection

@section('content')
    <div class="container w-11/12 md:pt-10 pb-10">
        <nav class="text-black font-bold my-8" aria-label="Breadcrumb">
            <ol class="list-none p-0 inline-flex">
                <li class="flex items-center">
                    <a href="/">首頁</a>
                    <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                        <path
                            d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                    </svg>
                </li>
                <li>
                    <a class="text-gray-500" aria-current="page">聯絡我們</a>
                </li>
            </ol>
        </nav>
        <div class="text-3xl mb-5 font-bold text-center">聯絡我們</div>
        <form class="w-2/3 mx-auto bg-white shadow p-8" action="{{ route('official.contact') }}" method="post">
            @if (session('message'))
                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4"
                     role="alert">
                    <strong class="font-bold">表單已送出!</strong>
                    <span class="block sm:inline">{{ session('message') }}</span>
                    <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                </span>
                </div>
            @endif
            @error('recaptcha')
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-3 rounded relative" role="alert">
                <span class="block sm:inline">{{ $message }}</span>
                <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                    <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 20 20">
                        <title>Close</title><path
                            d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
                </span>
            </div>
            @enderror
            @csrf
            <input type="hidden" id="recaptcha" name="recaptcha">
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="name">
                        姓名 <span class="text-red-500">*</span>
                    </label>
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white
                            @error('name') border-red-500 mb-3 @else border-gray-200 focus:border-gray-500 @enderror"
                           id="name" type="text" name="name" required value="{{ old('name') }}">
                    @error('name') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="type">
                        性別
                    </label>
                    <select id="type" name="type"
                            class="form-select @error('gender') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        <option value="">-- 請選擇 --</option>
                        @foreach(config('apaid.gender') as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                        @endforeach
                    </select>
                    @error('gender') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="email">
                        電子郵件 <span class="text-red-500">*</span>
                    </label>
                    <input id="email" name="email" type="email" value="{{ old('email') }}"
                           class="@error('email') border-red-500 mb-3 @else border-gray-200 focus:border-gray-500 @enderror appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                    @error('email') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="phone">
                        電話 <span class="text-red-500">*</span>
                    </label>
                    <input id="phone" name="phone" type="text" value="{{ old('phone') }}"
                           class="@error('phone') border-red-500 mb-3 @else border-gray-200 focus:border-gray-500 @enderror appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                    @error('phone') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="reply-method">
                        希望聯繫方式 <span class="text-red-500">*</span>
                    </label>
                    <select id="reply-method" name="reply_method"
                            class="form-select @error('reply_method') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        @foreach(config('apaid.contact.reply_method') as $key => $label)
                            <option @if(old('reply_method') == $key) selected @endif
                            value="{{ $key }}">{{ $label }}</option>
                        @endforeach
                    </select>
                    @error('reply_method') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="type">
                        問題類型 <span class="text-red-500">*</span>
                    </label>
                    <select id="type" name="type"
                            class="form-select @error('type') border-red-500 @else border-gray-200 focus:border-gray-500 @endif appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        <option value="" disabled>-- 請選擇 --</option>
                        @foreach($option['tags'] as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    @error('type') <p class="text-red-500 text-xs italic">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="content">
                        問題內容 <span class="text-red-500">*</span>
                    </label>
                    <textarea id="content" name="content"
                              class="no-resize appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 resize-none"
                              required>{{ old('content') }}</textarea>
                </div>
            </div>
            <div class="md:flex md:items-center align-items-end">
                <button
                    class="shadow bg-green-400 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-6 rounded-full"
                    type="submit">
                    送出
                </button>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.key') }}"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('{{ config('services.recaptcha.key') }}', {action: 'contact'}).then(function (token) {
                if (token) {
                    document.getElementById('recaptcha').value = token
                }
            });
        });
    </script>
@endsection
