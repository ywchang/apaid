@extends('layouts.admin')

@section('style')
    <style>
        tr td {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 350px;
        }
    </style>
@endsection


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 課程管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3 d-flex justify-content-between">
                <form>
                    <div class="input-group">
                        <input type="text" name="keyword" value="{{ request('keyword') }}" placeholder="課程名稱" class="form-control">
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary form-control" type="submit">搜尋</button>
                        </div>
                    </div>
                </form>
                <a href="{{ route('admin.courses.create') }}" class="btn btn-outline-linkedin">建立課程</a>
            </div>
            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>名稱</th>
                        <th>課程期間</th>
                        <th>課堂數</th>
                        <th>報名人數</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($courses as $key => $course)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $course->title }}</td>
                            <td>{{ $course->start_at->format('Y-m-d') }} - {{ $course->end_at->format('Y-m-d') }}</td>
                            <td>{{ $course->lessons()->count() }}</td>
                            <td>{{ $course->members()->count() }}</td>
                            <td>
                                <a href="{{ route('admin.courseLessons.index', $course->id) }}"><i class="mdi mdi-library-books menu-icon"></i> 管理</a>
                                <a class="ml-1" href="{{ route('admin.courses.edit', $course->id) }}"><i class="mdi mdi-lead-pencil menu-icon"></i> 編輯</a>
                                <a class="ml-1" href="{{ route('admin.courses.clone', $course->id) }}"><i class="mdi mdi-content-copy menu-icon"></i> 複製</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">還沒有課程哦！ <a href="{{ route('admin.courses.create') }}">開始建立課程</a></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $courses->links() }}
            </div>
        </div>
    </div>
@endsection
