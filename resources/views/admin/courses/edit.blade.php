@extends('layouts.admin')

@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.2.0/dist/style.css">

    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }

        .tags-input {
            margin-bottom: 0.5rem
        }

        .pointer {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 編輯課程 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.courses.index') }}">課程管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯課程</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                @include('admin.courses.form')

                <div class="d-flex justify-content-between">
                    <div>
                        <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                        </button>
                        <a class="btn btn-light" href="{{ route('admin.courses.index') }}">返回列表</a>
                    </div>
                    <div>
                        <button data-toggle="modal" data-target="#exampleModal" class="btn btn-danger">刪除課程
                        </button>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">刪除課程</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                確定要刪除 @{{ form.title }} ？
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                                <button type="button" @click.prevent="courseDelete" class="btn btn-inverse-danger">刪除
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <div v-else> 資料載入中...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {
                    members: []
                },
                options: {
                    members: [],
                    tags: []
                },
                tags: [],
                searchKeyword: null,
                errors: {},
                isLoaded: false
            },
            methods: {
                formSubmit() {
                    let vm = this;
                    let course = axios.put('./', this.form);
                    let courseMembers = axios.post('./members', {members: this.courseMembers})

                    Promise.all([course, courseMembers])
                        .then(res => {
                            Swal.fire({
                                title: '更新成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                            });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '更新失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
                afterSubmit(response) {
                    this.form = response.course;
                    this.errors = {}
                },
                uploadBanner(e) {
                    const vm = this;
                    let file = e.target.files[0];

                    const form = new FormData;
                    form.append('file', file);
                    form.append('filepath', 'uploads/banners');

                    axios.post('/admin/summernote/upload', form)
                        .then(xhr => vm.form.banner = xhr.data.filePath)
                        .finally(() => e.target.value = '');
                },
                removeMember(target) {
                    let index = this.form.members.findIndex(member => member.id === target.id);
                    this.form.members.splice(index, 1);
                },
                isCourseMember(target) {
                    return this.courseMembers.includes(target.id);
                },
                toggleCourseMember(target) {
                    if (this.isCourseMember(target)) {
                        let index = this.form.members.findIndex(member => member.id === target.id)
                        this.form.members.splice(index, 1);
                    } else {
                        this.form.members.push(target);
                    }
                },
                courseDelete() {
                    axios.delete(`/admin/courses/${this.form.id}`)
                        .then(() => {
                            Swal.fire({
                                title: '刪除成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.courses.index') }}'
                                }
                            })
                        })
                }
            },

            watch: {
                tags: function (tags) {
                    this.form.tags = tags.map(tag => tag.key)
                }
            },
            computed: {
                courseMembers() {
                    return this.form.members.map(member => member.id)
                },
                memberList() {
                    let members = this.options.members;

                    if (this.searchKeyword) {
                        let regex = new RegExp(this.searchKeyword, 'i');
                        members = members.filter(member => regex.exec(member.name) || regex.exec(member.code));
                    }

                    return members
                }
            },
            mounted() {
                const vm = this
                axios.get('./')
                    .then(res => {
                        vm.form = res.data.course
                        vm.tags = res.data.tags
                        vm.options = res.data.options
                    })
                    .finally(() => vm.isLoaded = true);
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
