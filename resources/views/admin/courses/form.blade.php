
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab"
           aria-controls="home" aria-selected="false">課程資訊</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="course-members-tab" data-toggle="tab" href="#course-members" role="tab"
           aria-controls="course-members" aria-selected="false">上課學員</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
        <div class="course-form">
            <div class="form-group">
                <label for="title">名稱 <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="title"
                       :class="{'is-invalid': errors.title }"
                       placeholder="課程名稱" v-model="form.title"/>
                <span class="invalid-feedback" v-if="errors.title" role="alert"><strong>@{{ errors.title[0] }}</strong></span>
            </div>
            <div class="form-group">
                <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model.boolean="form.is_displayed"> 顯示在課程列表 <i
                            class="input-helper"></i>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="teacher">講師 </label>
                <input type="text" class="form-control" id="teacher"
                       :class="{'is-invalid': errors.teacher }"
                       placeholder="講師" v-model="form.teacher"/>
                <span class="invalid-feedback" v-if="errors.teacher" role="alert"><strong>@{{ errors.teacher[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="address">上課地點 </label>
                <input type="text" class="form-control" id="address"
                       :class="{'is-invalid': errors.address }"
                       placeholder="上課地點" v-model="form.address"/>
                <span class="invalid-feedback" v-if="errors.address" role="alert"><strong>@{{ errors.address[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="price">報名費用 </label>
                <input type="number" class="form-control" id="price"
                       :class="{'is-invalid': errors.price }"
                       placeholder="報名費用" v-model="form.price"/>
                <span class="invalid-feedback" v-if="errors.price" role="alert"><strong>@{{ errors.price[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="summary">課程簡介</label>
                <summernote-textarea v-model="form.summary" v-if="isLoaded"></summernote-textarea>
            </div>
            <div class="form-group">
                <label for="start-at">課程開始日 <span class="text-danger">*</span></label>
                <input type="date" class="form-control" id="start-at"
                       :class="{ 'is-invalid': errors.start_at }"
                       v-model="form.start_at" placeholder="{{ now()->format('Y-m-d') }}">
                <span class="invalid-feedback" v-if="errors.start_at" role="alert"><strong>@{{ errors.start_at[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="end-at">課程截止日 <span class="text-danger">*</span></label>
                <input type="date" class="form-control" id="end-at"
                       :class="{ 'is-invalid' : errors.end_at }"
                       v-model="form.end_at" placeholder="{{ now()->format('Y-m-d') }}">
                <span class="invalid-feedback" v-if="errors.end_at" role="alert"><strong>@{{ errors.end_at[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="banner">橫幅圖片 </label>
                <div>
                    <img class="w-100 mb-2 img-thumbnail" :src="'/storage/' + form.banner" alt="course-banner" v-if="form.banner !== null">
                    <img class="w-100 mb-2 img-thumbnail" :src="'//place-hold.it/1440x480/cbd5e0/fff/4a5568?fontsize=48'" alt="course-banner" v-else>
                </div>
                <input type="file" class="form-control" id="banner" @change="uploadBanner" accept="image/*">
                <span class="invalid-feedback" v-if="errors.banner" role="alert"><strong>@{{ errors.banner[0] }}</strong></span>
                <small id="imageHelpBlock" class="form-text text-muted">
                    檔案上限為: {{ $max_upload_limit }} MB, 建議尺寸比例 1440x480
                </small>
            </div>
            <div class="form-group">
                <label for="tags">分類標籤</label>
                <v-tags-input type="text" class="" id="tags" v-model="tags"
                              placeholder="加入分類"
                              :existing-tags="this.options.tags"
                              :only-existing-tags="true"
                              :typeahead="true"
                              :typeahead-hide-discard="true"
                              :typeahead-always-show="true"
                ></v-tags-input>
                <small id="tagsHelpBlock" class="form-text text-muted">
                    沒有你要的分類嗎？請至 <a target="_blank" href="{{ route('admin.tags.index') }}">分類標籤管理</a> 建立新的<strong>課程分類</strong>
                </small>
            </div>

        </div>
    </div>
    <div class="tab-pane fade" id="course-members" role="tabpanel" aria-labelledby="course-members-tab">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-info btn-sm mb-3"
                data-toggle="modal"
                data-target="#memberModal">
            加入學員</button>

        <div style="overflow: scroll; max-height: 50vh">
            <table class="table table-hover mb-3">
                <thead>
                <tr>
                    <th>#</th>
                    <th>學員姓名</th>
                    <th>會員代號</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="form.members.length === 0">
                    <td colspan="4">目前沒有上課學員哦！</td>
                </tr>
                <tr v-else v-for="(member, index) of form.members">
                    <td>@{{ index + 1 }}</td>
                    <td>@{{ member.name }}</td>
                    <td>@{{ member.code }}</td>
                    <td><a href="#" class="btn btn-sm btn-outline-danger" @click.prevent="removeMember(member)"> 移除 </a></td>
                </tr>
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content bg-white">
                    <div class="modal-header">
                        <h5 class="modal-title" id="memberModalLabel">選擇上課學員</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-3">
                                <input type="text" class="form-control" placeholder="搜尋" v-model="searchKeyword"/>
                            </div>
                        </div>
                        <table class="table table-hover table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>會員代號</th>
                                <th>學員姓名</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-if="options.members.length === 0">
                                <td colspan="4">目前沒有學員哦！</td>
                            </tr>
                            <tr class="pointer" v-else v-for="(member, index) of memberList" @click="toggleCourseMember(member)">
                                <td>
                                    <input type="checkbox" :checked="isCourseMember(member)">
                                </td>
                                <td>@{{ member.code }}</td>
                                <td>@{{ member.name }}</td>
                                <td>
                                    <button class="btn btn-sm btn-outline-danger" v-if="isCourseMember(member)"> 移除 </button>
                                    <button class="btn btn-sm btn-outline-info" v-else> 加入 </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
