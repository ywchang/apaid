@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 檢視聯絡表單 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.contacts.index') }}">聯絡表單管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">檢視聯絡表單</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.contacts.update', $contact->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="course-form">
                    <div class="form-group">
                        <label for="name">名稱 </label>
                        <input type="text" id="name" name="name" placeholder="名稱" disabled
                               value="{{ $contact->name }}"
                               class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="type">問題類別 </label>
                        <input type="text" id="type" disabled
                               value="{{ $contact->types->name }}"
                               class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="gender">性別</label>
                        <div class="d-flex">
                            <div class="form-check form-check-info mr-4">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="1" @if($contact->gender == 1) checked @else disabled @endif> 男 <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="2" @if($contact->gender == 2) checked @else disabled @endif> 女 <i class="input-helper"></i></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">電話 </label>
                        <input type="text" id="phone" name="phone" placeholder="電話" disabled
                               value="{{ $contact->phone }}"
                               class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email </label>
                        <input type="text" id="email" name="email" placeholder="Email" disabled
                               value="{{ old('email', $contact->email) }}"
                               class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="reply-method">希望回覆方式</label>
                        <div class="d-flex">
                            <div class="form-check form-check-info mr-4">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="1"  @if($contact->reply_method == 1) checked @else disabled  @endif> 電話 <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-info mr-4">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="2"  @if($contact->reply_method == 2) checked @else disabled  @endif> Email <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-info">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" value="3"  @if($contact->reply_method == 3) checked @else disabled  @endif> 兩者皆可 <i class="input-helper"></i></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content">內容</label>
                        <textarea class="form-control" id="content" rows="4" disabled>{{ $contact->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="note">處理記錄 <span class="text-danger">*</span></label>
                        <textarea class="form-control" id="note" rows="4" name="note">{{ $contact->note }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="is_checked" @if($contact->is_checked) checked @endif> 已完成 <i class="input-helper"></i></label>
                        </div>
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">儲存</button>
                    <a class="btn btn-light" href="{{ route('admin.contacts.index') }}">返回</a>
                </div>
            </form>
        </div>
    </div>
@endsection
