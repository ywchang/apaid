@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 聯絡表單資料管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3"><a href="{{ route('admin.contacts.export') }}" class="btn btn-outline-linkedin">匯出聯絡表單</a>
            </div>
            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>問題類型</th>
                        <th>姓名</th>
                        <th>狀態</th>
                        <th>建立日期</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($contacts as $key => $contact)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $contact->types->name }}</td>
                            <td>{{ $contact->name }}</td>
                            <td>@if($contact->is_checked) <span class="text-secondary">完成</span> @else <span class="text-danger">未處理</span> @endif</td>
                            <td>{{ $contact->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.contacts.show', $contact->id) }}">檢視</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">還沒有聯絡表單資料哦！</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $contacts->links() }}
            </div>

        </div>
    </div>
@endsection
