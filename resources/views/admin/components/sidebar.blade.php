<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        {{--<li class="nav-item nav-profile">--}}
            {{--<a href="#" class="nav-link">--}}
                {{--<div class="nav-profile-image">--}}
                    {{--<img src="{{ asset("vendors/images/faces/face1.jpg") }}" alt="profile">--}}
                    {{--<span class="login-status online"></span>--}}
                    {{--<!--change to offline or busy as needed-->--}}
                {{--</div>--}}
                {{--<div class="nav-profile-text d-flex flex-column">--}}
                    {{--<span class="font-weight-bold mb-2">David Grey. H</span>--}}
                    {{--<span class="text-secondary text-small">Project Manager</span>--}}
                {{--</div>--}}
                {{--<i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.home') }}">
                <span class="menu-title">儀表版</span>
                <i class="mdi mdi-home menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.members.index') }}">
                <span class="menu-title">學員管理</span>
                <i class="mdi mdi-account menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.courses.index') }}">
                <span class="menu-title">課程管理</span>
                <i class="mdi mdi-library-books menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.about_us.index') }}">
                <span class="menu-title">關於我們管理</span>
                <i class="mdi mdi-crosshairs-gps menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.news.index') }}">
                <span class="menu-title">最新消息管理</span>
                <i class="mdi mdi-newspaper menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.events.index') }}">
                <span class="menu-title">學術活動管理</span>
                <i class="mdi mdi-newspaper menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.faqs.index') }}">
                <span class="menu-title">常見問題管理</span>
                <i class="mdi mdi-frequently-asked-questions menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.applications.index') }}">
                <span class="menu-title">申請表管理</span>
                <i class="mdi mdi-contact-mail menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.contacts.index') }}">
                <span class="menu-title">聯絡表單管理</span>
                <i class="mdi mdi-contact-mail menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.tags.index') }}">
                <span class="menu-title">分類標籤管理</span>
                <i class="mdi mdi-tag-multiple menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" target="_blank" href="{{ route('admin.docs') }}">
                <span class="menu-title">使用手冊</span>
                <i class="mdi mdi-book menu-icon"></i>
            </a>
        </li>
    </ul>
</nav>
