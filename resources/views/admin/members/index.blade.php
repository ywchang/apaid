@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 學員管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3 d-flex justify-content-between">
                <form>
                    <div class="input-group">
                        <input type="text" name="keyword" value="{{ request('keyword') }}" placeholder="姓名或會員編號" class="form-control">
                        <select class="form-control" name="role">
                            <option value="" selected>所有帳號角色</option>
                            @foreach(config('apaid.role') as $role => $label)
                                <option value="{{ $role }}" @if(request('role') === $role) selected @endif>{{ $label }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary form-control " type="submit">搜尋</button>
                        </div>
                    </div>
                </form>
                <div class="btn-group">
                    <a href="{{ route('admin.members.create') }}" class="btn btn-outline-linkedin">新增學員</a>
                    <a href="{{ route('admin.members.export') }}" class="btn btn-outline-linkedin">匯出學員</a>
                </div>
            </div>
            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>帳號</th>
                        <th>姓名</th>
                        <th>會員編號</th>
                        <th>帳號角色</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($members as $key => $member)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $member->username }}</td>
                            <td>{{ $member->name }}</td>
                            <td>{{ $member->code }}</td>
                            <td>{{ config('apaid.role')[$member->role] }}</td>
                            <td><a href="{{ route('admin.members.edit', $member->id) }}">編輯</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">還沒有學員哦！ <a href="{{ route('admin.members.create') }}">開始新增學員</a></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $members->links() }}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        let vueApp = {
            el: '#app',
            data: {
                paginator: {},
                members: []
            },
            mounted() {
                const vm = this;
                axios.get('members')
                    .then(response => {
                        console.log(response.data.members)
                        vm.paginator = response.data.members;
                        vm.members = vm.paginator.data;
                    });
            }
        }

        // const app = new Vue(vueApp);
    </script>
@endsection
