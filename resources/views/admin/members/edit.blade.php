@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }

        .form-check .form-check-label {
            padding-left: 1.75rem;
            margin-left: 0;
        }
        .cursor-move {
            cursor: move;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 編輯學員 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.members.index') }}">學員管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯學員</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            @include('admin.members.form')
            <div>
                <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                </button>
                <a class="btn btn-light" href="{{ route('admin.members.index') }}">返回列表</a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                options: {
                    paymentTitles: []
                },
                errors: {
                    "email.0": {}
                },
                isLoaded: false,
                showPassword: false,
            },

            methods: {
                formSubmit() {
                    const vm = this
                    axios.put('./', this.form)
                        .then(res => {
                            vm.errors = {};
                            Swal.fire({
                                title: '修改成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    if (res.data.redirect) {
                                        window.location = res.data.redirect
                                    }
                                }
                            })
                        })
                        .catch((error) => {
                            vm.errors = error.response.data.errors;
                            Swal.fire({
                                title: '修改失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            })
                        })
                },

                isTemporaryAccount() {
                    return this.form.role === 'temporary'
                },

                addPayment() {
                    this.form.payment_record.push({
                        date: null,
                        title: null,
                        method: null,
                        amount: 0,
                        note: null,
                    })
                },
                addPaymentTitle() {
                    axios.post('/admin/variable-settings/member_payment_title', {'value': this.options.paymentTitles })
                },
                removePaymentTitle(key) {
                    this.options.paymentTitles.splice(key, 1);
                },
                removePayment(target) {
                    let index = this.form.payment_record.findIndex(payment => target === payment)
                    this.form.payment_record.splice(index, 1);
                },
                previewAvatar(e) {
                    const vm = this;
                    let file = e.target.files[0];

                    const form = new FormData;
                    form.append('file', file);
                    form.append('filepath', 'uploads/avatars');

                    axios.post('/admin/summernote/upload', form)
                        .then(xhr => vm.form.avatar = xhr.data.filePath)
                        .finally(() => e.target.value = '');
                }
            },

            watch: {
                'form.role': function (roleValue) {
                    if ('temporary' !== roleValue) {
                        this.form.active_until = null
                    }
                }
            },
            computed: {
                passwordType() {
                    return this.showPassword ? 'text' : 'password'
                },
                avatar() {
                    return this.form.avatar
                        ? '/storage/' + this.form.avatar
                        : '/images/avatar-elite.jpg';
                }
            },
            mounted() {
                const vm = this;
                axios.get('./')
                    .then(res => vm.form = res.data.member)
                    .finally(() => vm.isLoaded = true)

                axios.get('/admin/variable-settings/member_payment_title')
                    .then(res => vm.options.paymentTitles = res.data.value)
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
