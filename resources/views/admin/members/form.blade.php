<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="account-tab" data-toggle="tab" href="#account" role="tab"
           aria-controls="account" aria-selected="false">帳號資訊</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
           aria-controls="profile" aria-selected="true">身份資訊</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab"
           aria-controls="payment" aria-selected="true">繳費資訊</a>
    </li>
</ul>

<div class="tab-content">
    <template v-if="isLoaded">
        <div class="tab-pane fade active show" id="account" role="tabpanel" aria-labelledby="account-tab">
            <div class="form-group">
                <label for="username">帳號 <span class="text-danger">*</span></label>
                <input type="text" id="username" placeholder="帳號" v-model="form.username"
                       class="form-control" :class="{'is-invalid': errors.username }">
                <span class="invalid-feedback" v-if="errors.username" role="alert"><strong>@{{ errors.username[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="password">密碼 </label>
                <input :type="passwordType" id="password" placeholder="密碼" v-model="form.password"
                       class="form-control" :class="{'is-invalid': errors.password }">
                <span class="invalid-feedback" v-if="errors.password" role="alert"><strong>@{{ errors.password[0] }}</strong></span>
                <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model="showPassword"> 顯示密碼 <i
                            class="input-helper"></i>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="role">帳號角色 <span class="text-danger">*</span></label>
                <select id="role" v-model="form.role"
                        class="form-control" :class="{'is-invalid': errors.role }">
                    <option :value="null" selected disabled>請選擇</option>
                    @foreach(config('apaid.role') as $role => $label)
                        <option value="{{ $role }}">{{ $label }}</option>
                    @endforeach
                </select>
                <span class="invalid-feedback" v-if="errors.role"
                      role="alert"><strong>@{{ errors.role[0] }}</strong></span>
            </div>
            <div class="form-group">
                <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model.boolean="form.active"> 啟用 <i
                            class="input-helper"></i>
                    </label>
                </div>
            </div>
            <div class="form-group" v-if="this.isTemporaryAccount()">
                <label for="active-until">帳號到期日 </label>
                <input type="date" class="form-control @error('active_until') is-invalid @enderror"
                       id="active-until"
                       v-model="form.active_until">
                <small id="emailHelp" class="form-text text-muted">如不設定自動到期，請略過此欄位</small>
            </div>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="form-group">
                <label for="name">姓名 <span class="text-danger">*</span></label>
                <input type="text" id="name" placeholder="姓名" v-model="form.name"
                       class="form-control" :class="{'is-invalid': errors.name }">
                <span class="invalid-feedback" v-if="errors.name"><strong>@{{ errors.name[0] }}</strong></span>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="code">會員代號 <span class="text-danger">*</span></label>
                        <input type="text" id="code" placeholder="會員代號" v-model="form.code"
                               class="form-control" :class="{'is-invalid': errors.code }">
                        <span class="invalid-feedback" v-if="errors.code"><strong>@{{ errors.code[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="birth">生日 <span class="text-danger">*</span></label>
                        <input type="date" id="birth" placeholder="{{ now()->format('Y-m-d') }}" v-model="form.birth"
                               class="form-control" :class="{'is-invalid': errors.birth }">
                        <span class="invalid-feedback" v-if="errors.birth"
                              role="alert"><strong>@{{ errors.birth[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="gender">性別 <span class="text-danger">*</span></label>
                        <select id="gender" v-model="form.gender"
                                class="form-control" :class="{'is-invalid': errors.gender }">
                            <option :value="null" selected disabled>請選擇</option>
                            <option :value="1">男</option>
                            <option :value="2">女</option>
                        </select>
                        <span class="invalid-feedback" v-if="errors.gender"
                              role="alert"><strong>@{{ errors.gender[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="email">聯絡信箱 <span class="text-danger">*</span></label>
                        <input-tag validate="email" :limit="5" v-model="form.email"></input-tag>
                        <input type="hidden" id="email" placeholder="聯絡信箱"
                               class="form-control" :class="{'is-invalid': errors['email.0'] }">
                        <span class="invalid-feedback" v-if="errors['email.0']"
                              role="alert"><strong>@{{ errors['email.0'][0] }}</strong></span>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            輸入逗號或按Enter分隔
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="address">聯絡地址 <span class="text-danger">*</span></label>
                        <input type="text" id="address" placeholder="聯絡地址" v-model="form.address"
                               class="form-control" :class="{'is-invalid': errors.address }">
                        <span class="invalid-feedback" v-if="errors.address"
                              role="alert"><strong>@{{ errors.address[0] }}</strong></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="facebook-name">Facebook 名稱 </label>
                        <input type="text" id="facebook-name" placeholder="Facebook 名稱" v-model="form.facebook_name"
                               class="form-control" :class="{'is-invalid': errors.facebook_name }">
                        <span class="invalid-feedback" v-if="errors.facebook_name"
                              role="alert"><strong>@{{ errors.facebook_name[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="line-id">Line ID </label>
                        <input type="text" id="line-id" placeholder="Line ID" v-model="form.line_id"
                               class="form-control" :class="{'is-invalid': errors.line_id }">
                        <span class="invalid-feedback" v-if="errors.line_id"
                              role="alert"><strong>@{{ errors.line_id[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="identity">身份 <span class="text-danger">*</span></label>
                        <input type="text" id="identity" placeholder="身份" v-model="form.identity"
                               class="form-control" :class="{'is-invalid': errors.identity }">
                        <span class="invalid-feedback" v-if="errors.identity"
                              role="alert"><strong>@{{ errors.identity[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="phone">聯絡電話 <span class="text-danger">*</span></label>
                        <input type="text" id="phone" placeholder="聯絡電話" v-model="form.phone"
                               class="form-control" :class="{'is-invalid': errors.phone }">
                        <span class="invalid-feedback" v-if="errors.phone"
                              role="alert"><strong>@{{ errors.phone[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="mobile">手機 <span class="text-danger">*</span></label>
                        <input type="text" id="mobile" placeholder="手機" v-model="form.mobile"
                               class="form-control" :class="{'is-invalid': errors.mobile }">
                        <span class="invalid-feedback" v-if="errors.mobile"
                              role="alert"><strong>@{{ errors.mobile[0] }}</strong></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="experience">學 / 經歷 <span class="text-danger">*</span></label>
                        <textarea type="text" id="experience" placeholder="經歷" rows="5" v-model="form.experience"
                                  class="form-control" :class="{'is-invalid': errors.experience }"></textarea>
                        <span class="invalid-feedback" v-if="errors.experience"
                              role="alert"><strong>@{{ errors.experience[0] }}</strong></span>
                        <div class="form-check form-check-flat form-check-primary">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" v-model="form.show_experience">
                                顯示學 / 經歷 <i class="input-helper"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="avatar">照片 </label>
                        <div>
                            <img class="img-thumbnail mb-1" :src="this.avatar" :alt="form.name + '的照片'" width="200">
                        </div>
                        <input type="file" placeholder="照片"
                               @change="previewAvatar"
                               class="form-control" :class="{'is-invalid': errors.avatar }">
                        <span class="invalid-feedback" v-if="errors.avatar"
                              role="alert"><strong>@{{ errors.avatar[0] }}</strong></span>
                        <small id="avatarHelpBlock" class="form-text text-muted">
                            檔案上限為: {{ $max_upload_limit }} MB, 建議尺寸大小為 200x250
                        </small>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <div class="form-check form-check-flat form-check-primary">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" v-model="form.show_current_job">
                                顯示現任職務資料 <i
                                    class="input-helper"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="clinic_name">診所名稱 / 職稱 <span class="text-danger">*</span></label>
                        <input type="text" id="clinic_name" placeholder="診所名稱 / 職稱" v-model="form.clinic_name"
                               class="form-control" :class="{'is-invalid': errors.clinic_name }">
                        <span class="invalid-feedback"
                              v-if="errors.clinic_name"><strong>@{{ errors.clinic_name[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label>服務區域 <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <select id="clinic_city" v-model="form.clinic_city" class="form-control" :class="{'is-invalid': errors.clinic_city }">
                                <option :value="null" selected disabled>請選擇</option>
                                @foreach(config('apaid.city') as $city)
                                    <option value="{{ $city }}">{{ $city }}</option>
                                @endforeach
                            </select>
                            <input type="text" id="clinic_district" placeholder="區域"
                                   v-model="form.clinic_district"
                                   class="form-control" :class="{'is-invalid': errors.clinic_district }">
                            <span class="invalid-feedback" v-if="errors.clinic_city"><strong>@{{ errors.clinic_city[0] }}</strong></span>
                            <span class="invalid-feedback" v-if="errors.clinic_district"><strong>@{{ errors.clinic_district[0] }}</strong></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="certificate_number">牙醫師證書編號 </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">牙字第</span>
                            </div>
                            <input type="text" id="certificate_number" placeholder="牙醫師證書編號"
                                   v-model="form.certificate_number"
                                   class="form-control" :class="{'is-invalid': errors.certificate_number }">
                            <div class="input-group-append">
                                <span class="input-group-text">號</span>
                            </div>
                            <span class="invalid-feedback"
                                  v-if="errors.certificate_number"><strong>@{{ errors.certificate_number[0] }}</strong></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="clinic_address">診所地址 <span class="text-danger">*</span></label>
                        <input type="text" id="clinic_address" placeholder="診所地址" v-model="form.clinic_address"
                               class="form-control" :class="{'is-invalid': errors.clinic_address }">
                        <span class="invalid-feedback"
                              v-if="errors.clinic_address"><strong>@{{ errors.clinic_address[0] }}</strong></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="note">備註 </label>
                <textarea id="note" rows="4" placeholder="備註" v-model="form.note"
                          class="form-control" :class="{'is-invalid': errors.note }"
                ></textarea>
                <span class="invalid-feedback"
                      v-if="errors.note"><strong>@{{ errors.note[0] }}</strong></span>
            </div>
            <div class="form-group">
                <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model.boolean="form.is_checked"> 資料已檢核
                        <i class="input-helper"></i>
                    </label>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
            <button type="button" class="btn btn-info btn-sm mb-3"
                    @click="addPayment">加入繳費
            </button>
            <button type="button" class="btn btn-secondary btn-sm mb-3" data-toggle="modal" data-target="#paymentTitleModal">
                項目設定
            </button>
            <div class="form-group">
                <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model.boolean="form.is_paid"> 已繳交常年會費
                        <i class="input-helper"></i>
                    </label>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>日期</th>
                        <th>項目</th>
                        <th>繳費方式</th>
                        <th>金額</th>
                        <th>備註</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-if="!form.payment_record || form.payment_record.length == 0">
                        <td colspan="6">沒有繳費紀錄</td>
                    </tr>
                    <tr v-for="payment of form.payment_record">
                        <td><input class="form-control p-2" type="date" v-model="payment.date" placeholder="{{ now()->format('Y-m-d') }}"/></td>
                        <td width="200">
                            <select class="form-control p-2" v-model="payment.title">
                                <option :value="null" disabled>請選擇</option>
                                <option v-for="title in options.paymentTitles" v-text="title"></option>
                            </select>
                        <td width="100">
                            <select class="form-control p-2" v-model="payment.method">
                                <option :value="null" disabled>請選擇</option>
                                <option>現金</option>
                                <option>匯款</option>
                            </select></td>
                        <td width="100"><input class="form-control p-2" type="number" v-model.number="payment.amount"></td>
                        <td>
                            <textarea class="form-control p-2" cols="30" rows="2" v-model="payment.note"></textarea>
                        </td>
                        <td width="50"><button class="btn btn-sm btn-inverse-danger" @click="removePayment(payment)">移除</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal fade" id="paymentTitleModal" tabindex="-1" role="dialog"
                 aria-labelledby="paymentTitleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="paymentTitleModalLabel">編輯項目名稱</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <button class="form-group btn btn-info btn-sm" @click="options.paymentTitles.push('')">增加</button>
                            <draggable :list="options.paymentTitles">
                                <div class="input-group mb-1" v-for="(title, key) of options.paymentTitles">
                                    <div class="input-group-prepend cursor-move">
                                        <span class="input-group-text bg-transparent" id="addon-wrapping"><i class="mdi mdi-cursor-move d-inline-block align-middle"></i></span>
                                    </div>
                                    <input class="form-control" v-model="options.paymentTitles[key]"/>
                                    <div class="input-group-append">
                                        <button class="btn btn-danger btn-sm" @click="removePaymentTitle(key)">刪除</button>
                                    </div>
                                </div>
                            </draggable>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"
                                    data-dismiss="modal"
                                    @click="addPaymentTitle">儲存
                            </button>

                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal"
                                    >關閉
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
    <span v-else> 資料準備中... </span>
</div>
