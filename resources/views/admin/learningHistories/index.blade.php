@extends('layouts.admin')

@section('style')
    <style>
        tr td {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 350px;
        }

        .pointer {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> {{ $course->title }} </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.courses.index') }}">課程管理</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.courseLessons.index', $course->id) }}">課堂管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">上課記錄</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3">
                <a href="{{ route('admin.courses.learningHistory.export', $course->id) }}"
                   class="btn btn-outline-linkedin">匯出上課紀錄</a>
            </div>
            <div>
                <table class="table table-hover mb-3 accordion">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>名稱</th>
                        <th></th>
                    </tr>
                    </thead>
                    @forelse($course->lessons as $key => $lesson)
                        <tr class="pointer"
                            data-toggle="collapse"
                            data-target=".collapse-{{ $lesson->code }}"
                            aria-controls="collapseLesson"
                            aria-expanded="false"
                        >
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $lesson->title }}</td>
                            <td>
                                <button class="btn btn-link btn-sm p-0"><i class="mdi mdi-eye"></i> 檢視記錄</button>
                            </td>
                        </tr>
                        <tr class="collapse collapse-{{ $lesson->code }}">
                            <td class="p-0" colspan="3">
                                <table class="table">
                                    <tr>
                                        <th>姓名</th>
                                        <th>教材</th>
                                        <th>完成時間</th>
                                    </tr>
                                    @foreach($course->members as $member)
                                        @foreach($lesson->materials as $material)
                                            @if($member->isComplete($material))
                                                <tr>
                                                    <td>{{ $member->name }}</td>
                                                    <td>{{ $material->name }}</td>
                                                    <td>{{ $material->members->where('id', $member->id)->first()->pivot->completed }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </table>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">沒有課堂資料</td>
                        </tr>
                    @endforelse
                </table>
                <a class="btn btn-light" href="{{ route('admin.courseLessons.index', $course->id) }}">回到課堂管理</a>
            </div>
        </div>
    </div>
@endsection
