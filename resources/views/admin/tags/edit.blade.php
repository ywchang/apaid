@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 編輯分類標籤 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.tags.index') }}">分類標籤管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯分類標籤</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.tags.update', $tag->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="course-form">
                    <div class="form-group">
                        <label for="name">名稱 <span class="text-danger">*</span></label>
                        <input type="text" id="name" name="name" placeholder="標籤名稱"
                               value="{{ old('name', $tag->name) }}"
                               class="form-control @error('name') is-invalid @enderror"/>
                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div>
                        <button type="submit" class="btn btn-gradient-primary mr-2">送出表單</button>
                        <a class="btn btn-light" href="{{ route('admin.tags.index') }}">返回列表</a>
                    </div>
                    <div>
                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger">刪除分類標籤</button>
                    </div>
                </div>
            </form>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">刪除分類標籤</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            確定要刪除 {{ $tag->name }} ？
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                            <button type="button" onclick="deleteTag()" class="btn btn-inverse-danger">刪除</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteTag() {
            axios.delete("{{ route('admin.tags.destroy', $tag->id) }}")
                .then(() => {
                    Swal.fire({
                        title: '刪除成功!',
                        text: '',
                        timer: 2000,
                        icon: 'success',
                        confirmButtonText: '確定',
                        onClose: () => {
                            window.location = '{{ route('admin.tags.index') }}'
                        }
                    })
                })
        }
    </script>
@endsection
