@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }

    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 分類標籤管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="news-tab" data-toggle="tab" href="#news" role="tab"
                       aria-controls="news" aria-selected="false">最新消息分類</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="course-tab" data-toggle="tab" href="#course" role="tab"
                       aria-controls="course" aria-selected="true">課程分類</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                       aria-controls="contact" aria-selected="true">聯絡表單分類</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active show" id="news" role="tabpanel" aria-labelledby="news-tab">
                    <div class="mb-3"><a href="{{ route('admin.tags.create', ['type' => 'news']) }}" class="btn btn-outline-linkedin">新增分類</a></div>
                    <tags-sortable-table
                        :list="this.newsTags"
                        v-on:sorted="handleSorting"
                        href="{{ route('admin.tags.create', ['type' => 'news']) }}"
                    ></tags-sortable-table>
                </div>
                <div class="tab-pane fade" id="course" role="tabpanel" aria-labelledby="course-tab">
                    <div class="mb-3"><a href="{{ route('admin.tags.create', ['type' => 'course']) }}" class="btn btn-outline-linkedin">新增分類</a>
                    </div>
                    <tags-sortable-table
                        :list="this.courseTags"
                        v-on:sorted="handleSorting"
                        href="{{ route('admin.tags.create', ['type' => 'course']) }}"
                    ></tags-sortable-table>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="mb-3"><a href="{{ route('admin.tags.create', ['type' => 'contact']) }}" class="btn btn-outline-linkedin">新增分類</a>
                    </div>
                    <tags-sortable-table
                        :list="this.contactTags"
                        v-on:sorted="handleSorting"
                        href="{{ route('admin.tags.create', ['type' => 'contact']) }}"
                    ></tags-sortable-table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        app = new Vue({
            "el": "#app",
            data: {
                tags: []
            },
            computed: {
                courseTags() {
                    return this.tags.filter(tag => tag.type === "App\\Course")
                },
                newsTags() {
                    return this.tags.filter(tag => tag.type === "App\\News")
                },
                contactTags() {
                    return this.tags.filter(tag => tag.type === "App\\Contact")
                }
            },
            methods: {
                handleSorting(items) {
                    axios.post('/admin/tags/sort', {
                        tags: items.map(item => item.id)
                    });
                }
            },
            mounted() {
                this.tags = @json($tags)
            }
        });

    </script>


@endsection
