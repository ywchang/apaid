@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 建立分類標籤 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.tags.index') }}">分類標籤管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯分類標籤</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.tags.store') }}" method="post">
                @csrf
                <div class="course-form">
                    <div class="form-group">
                        <label for="name">名稱 <span class="text-danger">*</span></label>
                        <input type="text" id="name" name="name" placeholder="標籤名稱"
                               value="{{ old('name') }}"
                               class="form-control @error('name') is-invalid @enderror"/>
                        @error('name')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="type" name="type" placeholder="類型"
                               class="form-control @error('type') is-invalid @enderror"
                               @switch(request('type'))
                                   @case('news')
                                      @php($type = App\News::class)
                                      @break
                                   @case('course')
                                      @php($type = App\Course::class)
                                      @break
                                   @case('contact')
                                       @php($type = App\Contact::class)
                                       @break
                                   @default
                                      @php($type = '')
                               @endswitch
                                value="{{ $type }}"/>
                        @error('type')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">送出表單</button>
                    <a class="btn btn-light" href="{{ route('admin.tags.index') }}">返回列表</a>
                </div>
            </form>
        </div>
    </div>
@endsection
