@extends('layouts.admin')

@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.2.0/dist/style.css">

    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }
        .tags-input {
            border: 1px solid #ebedf2;
            border-radius: 0;
            margin-bottom: 0.5rem;
            padding: 0.75rem 1.375rem;
        }

        .tags-input input::placeholder {
            font-size:0.8125rem
        }

        .is-invalid > .tags-input {
            border-color: #dc3545;
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23dc3545' viewBox='-2 -2 7 7'%3e%3cpath stroke='%23dc3545' d='M0 0l3 3m0-3L0 3'/%3e%3ccircle r='.5'/%3e%3ccircle cx='3' r='.5'/%3e%3ccircle cy='3' r='.5'/%3e%3ccircle cx='3' cy='3' r='.5'/%3e%3c/svg%3E");
            background-repeat: no-repeat;
            background-position: center right calc(0.375em + 0.1875rem);
            background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem);
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 建立最新消息 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.news.index') }}">最新消息管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">建立最新消息</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                @include('admin.news.form')
                <div>
                    <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                    </button>
                    <a class="btn btn-light" href="{{ route('admin.news.index') }}">返回列表</a>
                </div>
            </template>
            <div v-else> 資料準備中...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                errors: {},
                tags: [],
                options: {},
                isLoaded: false
            },

            methods: {
                formSubmit() {
                    const vm = this;
                    axios.post('./', this.form)
                        .then(response => {
                            Swal.fire({
                                title: '建立成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.news.index') }}'
                                }
                            })
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '建立失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
                uploadBanner(e) {
                    const vm = this;
                    let file = e.target.files[0];

                    const form = new FormData;
                    form.append('file', file);
                    form.append('filepath', 'uploads/news');

                    axios.post('/admin/summernote/upload', form)
                        .then(xhr => vm.form.image = xhr.data.filePath)
                        .finally(() => {
                            e.target.value = ''
                        });
                },
            },
            watch: {
                tags: function (tags) {
                    this.form.tags = tags.map(tag => tag.key)
                }
            },
            mounted() {
                const vm = this;

                axios.get('./new')
                    .then(res => {
                        vm.form = res.data.news
                        vm.options = res.data.options;
                        vm.isLoaded = true
                    })
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
