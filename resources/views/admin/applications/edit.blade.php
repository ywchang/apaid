@extends('layouts.admin')


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> {{ $mapper[$application->post_name] }}管理 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.applications.index') }}">申請表管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯{{ $mapper[$application->post_name] }}</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                <div>
                    <div class="form-group">
                        <label for="content">內容</label>
                        <summernote-textarea v-model="form.content" :name="'content'" v-if="isLoaded"></summernote-textarea>
                    </div>

                    <div class="form-group">
                        <label for="attachment">附件上傳 </label>
                        <input type="file" class="form-control" id="attachment" @change="uploadHandler">
                        <small class="form-text text-muted">附件上傳大小上限為 {{ min((int)ini_get("post_max_size"), (int)ini_get("upload_max_filesize")) }} MB</small>
                    </div>

                    <table class="table table-striped mb-4">
                        <tr>
                            <td>#</td>
                            <td>檔名</td>
                            <td>操作</td>
                        </tr>
                        <tr v-for="(attachment, key) of form.attachments">
                            <td>@{{ key + 1 }}</td>
                            <td>
                                <input type="text" class="form-control" @change="renameHandler(attachment, $event)" v-model.lazy="attachment.name">
                            </td>
                            <td>
                                <a class="btn btn-link btn-sm" target="_blank" :href="'/storage/' + attachment.path">下載</a>
                                <a class="btn btn-danger btn-sm" href="#" @click.prevent="removeHandler(attachment)">刪除</a>
                            </td>
                        </tr>
                    </table>

                    <div>
                        <button type="button" @click="this.formSubmit" class="btn btn-gradient-primary mr-2">送出表單</button>
                        <a class="btn btn-light" href="{{ route('admin.applications.index') }}">返回列表</a>
                    </div>

                </div>
            </template>
            <div v-else>資料載入中 ...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {
                    attachments: []
                },
                errors: {},
                isLoaded: false
            },
            methods: {
                formSubmit() {
                    let vm = this;
                    axios.put('./', this.form)
                        .then(res => {
                            Swal.fire({
                                title: '更新成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                            });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '更新失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
                uploadHandler(event) {
                    let vm = this;
                    let form = new FormData;
                    form.append('file', event.target.files[0]);
                    axios.post('/admin/attachments', form, {headers: {'Content-Type': 'multipart/form-data'}})
                        .then(response => {
                            vm.form.attachments.push(response.data.attachment);
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '上傳失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });
                        })
                        .finally(() => {
                            event.target.value = ''

                        })
                },
                renameHandler(target, event) {
                    let name = event.target.value;
                    axios.put(`/admin/attachments/${target.id}`, {
                        'name' : name
                    })
                },
                removeHandler(target) {
                    if (confirm('確定要刪除？')) {
                        let index = this.form.attachments.findIndex(attachment => attachment === target);
                        this.form.attachments.splice(index, 1);
                        axios.delete(`/admin/attachments/${target.id}`);
                    }
                }
            },
            mounted() {
                const vm = this
                axios.get('./')
                    .then(res => vm.form = res.data.application)
                    .finally(() => vm.isLoaded = true);
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
