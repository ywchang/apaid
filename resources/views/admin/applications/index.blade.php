@extends('layouts.admin')

@section('style')
    <style>
        tr td {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 350px;
        }
    </style>
@endsection


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 申請表管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>標題</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($applications as $key => $application)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $mapper[$application->post_name] }}</td>
                            <td>
                                <a class="ml-1" href="{{ route('admin.applications.edit', $application->id) }}"><i class="mdi mdi-lead-pencil menu-icon"></i> 編輯</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
