@extends('layouts.admin')

@section('style')
    <style>
        tr td {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 350px;
        }
        .cursor-pointer {
            cursor: pointer;
        }
        .table-hover tbody tr.cursor-pointer:hover {
            background-color: lightgray;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> {{ $course->title }} </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.courses.index') }}">課程管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">課堂管理</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3">
                <a href="{{ route('admin.courses.edit', $course->id) }}" class="btn btn-outline-success">編輯課程</a>
                <div class="dropdown d-inline-block">
                    <a class="btn btn-outline-linkedin dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        增加課堂
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.courseLessons.create', $course->id) }}">新增課堂</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#lessonModal">選擇現有課堂</a>
                    </div>
                </div>
                <a href="{{ route('admin.courses.learningHistory', $course->id) }}"
                   class="btn btn-outline-linkedin">上課紀錄</a>
            </div>

            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>名稱</th>
                        <th>講師</th>
                        <th>上課時間</th>
                        <th>教材數</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($lessons as $key => $lesson)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $lesson->title }}</td>
                            <td>{{ $lesson->teacher }}</td>
                            <td>{{ $lesson->start_at->format('m月d日') }} ~ {{ $lesson->end_at->format('m月d日') }}</td>
                            <td>{{ $lesson->materials()->count() }}</td>
                            <td><a href="{{ route('admin.courseLessons.edit', [$course->id, $lesson->id]) }}">
                                    <i class="mdi mdi-lead-pencil menu-icon"></i> 編輯</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">還沒有課堂資料哦！<a href="{{ route('admin.courseLessons.create', $course->id) }}">開始新增課堂</a>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $lessons->links() }}
                <a class="btn btn-light" href="{{ route('admin.courses.index') }}">回到課程管理</a>
            </div>

            <div class="modal fade" id="lessonModal" tabindex="-1" aria-labelledby="lessonModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="lessonModalLabel">選擇現有課堂</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover mb-4">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>名稱</th>
                                    <th>課堂代號</th>
                                    <th>引用課堂數</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-if="this.lessons.length === 0">
                                    <td>沒有課堂資料</td>
                                </tr>
                                <tr v-for="lesson of this.lessons" class="cursor-pointer" @click="toggleSelect(lesson)">
                                    <td><input type="checkbox" :checked="isSelected(lesson)"></td>
                                    <td>@{{ lesson.title }}</td>
                                    <td>@{{ lesson.code }}</td>
                                    <td>@{{ lesson.courses_count }}</td>
                                    <td>
                                        <a href="#" class="text-danger"
                                           @click.prevent.stop="removeLesson(lesson)"><i class="mdi mdi mdi-delete menu-icon"></i> 刪除
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">
                                <a class="btn btn-outline-info mr-1" :class="{ 'disabled' : !this.hasPreviousPage }" @click.prevent="previousPage" href="#">上一頁</a>
                                <a class="btn btn-outline-info" :class="{ 'disabled' : !this.hasNextPage }" @click.prevent="nextPage" href="#">下一頁</a>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                            <button type="button" class="btn btn-primary" @click="formSubmit" >加入</button>
                        </div>
                    </div>
                </div>
                <form id="add-lesson-form" method="POST" action="{{ route('admin.courseLessons.add', $course->id) }}">
                    @csrf
                    <input type="hidden" name="lessons[]" :value="id" v-for="id in this.selectedIds">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        let lessonModal = new Vue({
            el: '#lessonModal',
            data: {
                selected: [],
                paginator: {},
                lessons: [],
            },
            methods: {
                isSelected(target) {
                    return this.selectedIds.includes(target.id);
                },
                toggleSelect(target) {
                    let index = this.selected.findIndex(lesson => lesson === target);
                    if (index >= 0) {
                        this.selected.splice(index, 1);
                    } else {
                        this.selected.push(target)
                    }
                },

                previousPage() {
                    axios.get(this.paginator.prev_page_url)
                        .then(res => {
                            this.paginator = res.data.lessons
                            this.lessons = this.paginator.data
                        })
                },
                nextPage() {
                    axios.get(this.paginator.next_page_url)
                        .then(res => {
                            this.paginator = res.data.lessons
                            this.lessons = this.paginator.data
                        })
                },
                formSubmit() {
                    document.getElementById("add-lesson-form").submit()
                },
                removeLesson(target) {
                    if (confirm(`確定要刪除課堂 ${target.title} ？\n(不可復原)`)) {

                        let index = this.lessons.findIndex(lesson => lesson.id === target.id);
                        if (index > -1) {
                            this.lessons.splice(index, 1);
                        }

                        axios.delete(`/admin/lessons/${target.id}`)
                        alert('已刪除');
                    }
                }
            },
            computed: {
                selectedIds() {
                    return this.selected.map(lesson => lesson.id)
                },
                hasPreviousPage() {
                    return this.paginator.current_page > 1
                },
                hasNextPage() {
                    return this.paginator.current_page < this.paginator.last_page
                }
            },
            mounted() {
                axios.get('/admin/lessons')
                .then(res => {
                    this.paginator = res.data.lessons
                    this.lessons = this.paginator.data
                })
            }
        })
    </script>
@endsection
