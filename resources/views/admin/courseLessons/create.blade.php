@extends('layouts.admin')

@section('style')
    <style>
        .tab-content {
            border-top: 0;
            padding: 2rem 0;
            text-align: justify;
        }

        #materials .menu-icon.edit {

            visibility: hidden;
        }
        #materials tr:hover .menu-icon.edit {
            visibility: visible;
        }

        .cursor-move {
            cursor: move;
        }

        .cursor-pointer {
            cursor: pointer;
        }
        .table tbody tr.cursor-pointer:hover {
            background-color: lightgray;
        }
    </style>
@endsection

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 建立課堂 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">課程管理</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{ route('admin.courseLessons.index', $course->id) }}">{{ $course->title }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">建立課堂</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            @include('admin.courseLessons.form')

            <div>
                <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                </button>
                <a class="btn btn-light" href="{{ route('admin.courseLessons.index', $course->id) }}">返回列表</a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {
                    materials: [],
                },
                paginator: {},
                materials: [],
                targetMaterial: {},
                errors: {},
                isProcessing: false,
                isLoaded: false,
            },

            methods: {
                formSubmit() {
                    const vm = this;
                    axios.post('./', this.form)
                        .then(response => {
                            Swal.fire({
                                title: '建立成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.courseLessons.index', $course->id) }}'
                                }
                            })
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '建立失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });
                            vm.errors = error.response.data.errors
                        })
                },
                selectFile() {
                    $(".file-upload-default").trigger('click')
                },
                upload(event) {
                    const vm = this;
                    let file = event.target.files[0];
                    if (!file) return;
                    this.isProcessing = true;
                    VideoUploader.send(file)
                        .then(xhr => {
                            let material = Object.assign({
                                'name': xhr.responseJson.file.origin_name
                            }, xhr.responseJson.file);

                            return axios.post('/admin/materials', material)
                        })
                        .then((xhr) => {
                            let material = xhr.data.material;

                            if (!vm.form.materials) {
                                vm.form.materials = []
                            }

                            vm.form.materials.push(material)
                        })
                        .catch(err => console.log(err))
                        .finally(() => vm.isProcessing = false)
                },
                editMaterial(target) {
                    this.targetMaterial = target;
                    $("#editMaterialModal").modal()
                },
                updateMaterial() {
                    axios.put(`/admin/materials/${this.targetMaterial.id}`, this.targetMaterial);
                },
                getIconClass(material) {
                    switch (material.type) {
                        case 'pdf':
                            return 'mdi-file-pdf';
                        case 'video':
                            return 'mdi-video';
                        case 'image':
                            return 'mdi-file-image';
                        default:
                            return ''
                    }
                },
                removeMaterial(target) {
                    if (confirm(`確定要刪除 ${target.name} 教材？\n(不可復原)`)) {
                        let index = this.form.materials.findIndex(material => material.id === target.id);
                        if (index > -1) {
                            this.form.materials.splice(index, 1);
                        }

                        index = this.materials.findIndex(material => material.id === target.id);
                        if (index > -1) {
                            this.materials.splice(index, 1);
                        }

                        axios.delete(`/admin/materials/${target.id}`)
                        alert('教材已刪除');
                    }
                },
                isSelected(target) {
                    return this.selectedIds.includes(target.id);
                },
                toggleSelect(target) {
                    let index = this.form.materials.findIndex(material => material.id === target.id);
                    if (index >= 0) {
                        this.form.materials.splice(index, 1);
                    } else {
                        this.form.materials.push(target)
                    }
                },

                previousPage() {
                    axios.get(this.paginator.prev_page_url)
                        .then(res => {
                            this.paginator = res.data.materials
                            this.materials = this.paginator.data
                        })
                },
                nextPage() {
                    axios.get(this.paginator.next_page_url)
                        .then(res => {
                            this.paginator = res.data.materials
                            this.materials = this.paginator.data
                        })
                },
            },
            computed: {
                selectedIds() {
                    return this.form.materials.map(material => material.id)
                },
                hasPreviousPage() {
                    return this.paginator.current_page > 1
                },
                hasNextPage() {
                    return this.paginator.current_page < this.paginator.last_page
                }
            },
            mounted() {
                const vm = this;
                axios.get('./new')
                    .then(res => vm.form = res.data.lesson)
                    .finally(() => vm.isLoaded = true);

                axios.get('/admin/materials')
                    .then(res => {
                        this.paginator = res.data.materials
                        this.materials = this.paginator.data
                    })
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
