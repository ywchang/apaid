<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="lesson-tab" data-toggle="tab" href="#lesson-1" role="tab"
           aria-controls="lesson" aria-selected="false">課堂資訊</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="materials-tab" data-toggle="tab" href="#materials" role="tab"
           aria-controls="materials" aria-selected="false">教材</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active show" id="lesson-1" role="tabpanel" aria-labelledby="lesson-tab">
        <div class="lesson-form">
            <div class="form-group">
                <label for="title">名稱 <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="title"
                       :class="{'is-invalid': errors.title }"
                       placeholder="名稱" v-model="form.title"/>
                <span class="invalid-feedback" v-if="errors.title"
                      role="alert"><strong>@{{ errors.title[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="code">課堂代碼 <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="code"
                       :class="{'is-invalid': errors.code }"
                       placeholder="課堂代碼" v-model="form.code"/>
                <span class="invalid-feedback" v-if="errors.code"
                      role="alert"><strong>@{{ errors.code[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="teacher">講師 <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="teacher"
                       :class="{'is-invalid': errors.teacher }"
                       placeholder="講師" v-model="form.teacher"/>
                <span class="invalid-feedback" v-if="errors.teacher"
                      role="alert"><strong>@{{ errors.teacher[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="start-at">課程開始日 <span class="text-danger">*</span></label>
                <input type="date" class="form-control" id="start-at"
                       :class="{ 'is-invalid': errors.start_at }"
                       v-model="form.start_at" placeholder="{{ now()->format('Y-m-d') }}">
                <span class="invalid-feedback" v-if="errors.start_at" role="alert"><strong>@{{ errors.start_at[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="end-at">課程截止日 <span class="text-danger">*</span></label>
                <input type="date" class="form-control" id="end-at"
                       :class="{ 'is-invalid' : errors.end_at }"
                       v-model="form.end_at" placeholder="{{ now()->format('Y-m-d') }}">
                <span class="invalid-feedback" v-if="errors.end_at" role="alert"><strong>@{{ errors.end_at[0] }}</strong></span>
            </div>
            <div class="form-group">
                <label for="syllabus">大綱</label>
                <summernote-textarea v-model="form.syllabus" v-if="isLoaded"></summernote-textarea>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="materials" role="tabpanel" aria-labelledby="materials-tab">
        <div class="form-group">
            <label for="file"> 上傳教材 </label>
            <input type="file" id="file" class="file-upload-default" @change="upload"
                   accept="video/mp4,application/pdf,image/*">

            <div class="col-xs-12">
                <button class="btn btn-info" @click="selectFile" type="button">上傳</button>
                <button class="btn btn-info" type="button" data-toggle="modal" data-target="#selectMaterialModal" >選擇現有教材</button>
            </div>
        </div>

        <sortable-table :list.sync="form.materials">
            <template v-slot:thead>
                <tr>
                    <th>#</th>
                    <th>名稱</th>
                    <th>檔案大小</th>
                    <th>操作</th>
                </tr>
            </template>
            <template v-slot:tbody>
                <tr v-if="form.materials.length === 0">
                    <td colspan="4">沒有教材記錄</td>
                </tr>
                <tr v-else v-for="(material, index) of form.materials">
                    <td><i class="mdi mdi-cursor-move cursor-move"></i></td>
                    <td class="cursor-pointer" @click="editMaterial(material)">
                        <span v-text="material.name"></span>
                        <i class="edit mdi mdi-lead-pencil menu-icon"></i>
                        <span class="text-danger" v-if="!material.is_exist">(檔案遺失)</span>
                    </td>
                    <td><i class="mdi menu-icon" :class="getIconClass(material)"></i> @{{ material.size |
                        prettyBytes }}
                    </td>
                    <td>
                        <a href="#" class="text-danger"
                           @click.prevent="toggleSelect(material)"><i
                                class="mdi mdi-delete menu-icon"></i> 移除</a>
                    </td>
                </tr>
            </template>
        </sortable-table>

        <div class="progress progress-md" v-if="isProcessing">
            <div class="progress-bar bg-info progress-bar-striped progress-bar-animated"
                 role="progressbar" aria-valuenow="0" aria-valuemin="0"
                 aria-valuemax="100"></div>
        </div>
        <div class="modal fade" id="editMaterialModal" tabindex="-1" role="dialog"
             aria-labelledby="editMaterialModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="materialModalLabel">編輯名稱</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" class="form-control"
                               v-model="targetMaterial.name"
                               placeholder="選擇教材">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal"
                                @click="updateMaterial">關閉
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="selectMaterialModal" tabindex="-1" aria-labelledby="selectMaterialModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="selectMaterialModalLabel">選擇現有教材</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>名稱</th>
                                <th>類型</th>
                                <th>引用次數</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-if="this.materials.length === 0">
                                <td>沒有課堂資料</td>
                            </tr>
                            <tr v-for="material of this.materials" class="cursor-pointer" @click="toggleSelect(material)">
                                <td><input type="checkbox" :checked="isSelected(material)"></td>
                                <td>@{{ material.name }}</td>
                                <td><i class="mdi menu-icon" :class="getIconClass(material)"></i>@{{ material.size | prettyBytes }}</td>
                                <td>@{{ material.lessons_count }}</td>
                                <td>
                                    <a href="#" class="text-danger"
                                       @click.prevent.stop="removeMaterial(material)"><i
                                            class="mdi mdi-delete menu-icon"></i> 刪除</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center">
                            <a class="btn btn-outline-info mr-1" :class="{ 'disabled' : !this.hasPreviousPage }" @click.prevent="previousPage" href="#">上一頁</a>
                            <a class="btn btn-outline-info" :class="{ 'disabled' : !this.hasNextPage }" @click.prevent="nextPage" href="#">下一頁</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
