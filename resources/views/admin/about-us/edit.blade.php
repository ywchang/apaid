@extends('layouts.admin')


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> {{ $title }}管理 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.about_us.index') }}">關於我們管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯{{ $title }}頁面</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                <div>
                    <div class="form-group">
                        <label for="content">內容</label>
                        <summernote-textarea v-model="form.content" :name="'content'" v-if="isLoaded"></summernote-textarea>
                    </div>

                    <div>
                        <button type="button" @click="this.formSubmit" class="btn btn-gradient-primary mr-2">送出表單</button>
                        <a class="btn btn-light" href="{{ route('admin.about_us.index') }}">返回列表</a>
                    </div>

                </div>
            </template>
            <div v-else>資料載入中 ...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                errors: {},
                isLoaded: false
            },
            methods: {
                formSubmit() {
                    let vm = this;
                    axios.put('./', this.form)
                        .then(res => {
                            Swal.fire({
                                title: '更新成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                            });
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '更新失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
            },
            mounted() {
                const vm = this
                axios.get('./')
                    .then(res => vm.form = res.data.post)
                    .finally(() => vm.isLoaded = true);
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
