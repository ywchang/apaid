<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('vendors/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('vendors/images/favicon.png') }}"/>
    <style>
        .auth .brand-logo {
            margin-bottom: 1.3rem;
        }
        .auth .brand-logo img {
            width: 35px;
            max-width: 100%;
            height: 33px;
        }
        .auth .brand-logo span {
            font-size: 1.8rem;
        }
    </style>
</head>
<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
            <div class="row flex-grow">
                <div class="col-lg-4 mx-auto">
                    <div class="auth-form-light text-left p-5">
                        <div class="brand-logo d-flex align-items-center">
                            <img src="{{ asset('vendors/images/logo-mini.svg') }}">
                            <span class="text-primary text-no ml-2">APAID 管理後台</span>
                        </div>
                        <form class="pt-3" method="POST" action="{{ route('admin.login') }}">
                            @csrf
                            <div class="form-group">
                                <input type="email"
                                       class="form-control form-control-lg @error('email') is-invalid @enderror"
                                       id="email"
                                       name="email"
                                       required
                                       placeholder="信箱">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="password"
                                       class="form-control form-control-lg"
                                       id="password"
                                       name="password"
                                       required
                                       placeholder="密碼">
                            </div>
                            <div class="mt-3">
                                <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
                                        type="submit">登入
                                </button>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <input type="checkbox"
                                               class="form-check-input"
                                               name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}> 記住我
                                    </label>
                                </div>
                                @if (false && Route::has('admin.password.request'))
                                    <a href="{{ route('admin.password.request') }}" class="auth-link text-black">Forgot
                                        password?</a>
                                @endif
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
