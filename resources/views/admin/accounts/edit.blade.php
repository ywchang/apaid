@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> 編輯帳號 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                <form>
                    <div class="form-group">
                        <label for="name">帳號 <span class="text-danger">*</span></label>
                        <input type="text" id="name" placeholder="帳號" :value="form.name"
                               class="form-control" readonly >
                    </div>
                    <div class="form-group">
                        <label for="email">Email <span class="text-danger">*</span></label>
                        <input type="email" id="email" placeholder="帳號" v-model="form.email"
                               class="form-control" :class="{'is-invalid': errors.email }">
                        <span class="invalid-feedback" v-if="errors.email" role="alert"><strong>@{{ errors.email[0] }}</strong></span>
                    </div>
                    <div class="form-group">
                        <label for="password">密碼 </label>
                        <input :type="passwordType" id="password" placeholder="密碼" v-model="form.password"
                               class="form-control" :class="{'is-invalid': errors.password }">
                        <span class="invalid-feedback" v-if="errors.password" role="alert"><strong>@{{ errors.password[0] }}</strong></span>
                        <div class="form-check form-check-flat form-check-primary">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" v-model="showPassword"> 顯示密碼 <i
                                    class="input-helper"></i>
                            </label>
                        </div>
                    </div>
                </form>
                <div class="d-flex justify-content-between">
                    <div>
                        <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                        </button>
                    </div>
                </div>
            </template>
            <div v-else> 資料載入中...</div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                errors: {},
                showPassword: false,
                isLoaded: false
            },

            methods: {
                formSubmit() {
                    const vm = this;
                    axios.put('./', this.form)
                        .then(response => {
                            vm.errors = {};
                            vm.form = response.data.user;

                            Swal.fire({
                                title: '更新成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                            })
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '更新失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
            },
            computed: {
                passwordType() {
                    return this.showPassword ? 'text' : 'password'
                },
            },
            mounted() {
                const vm = this;

                axios.get('./')
                    .then(res => {
                        vm.form = res.data.user;
                        vm.isLoaded = true
                    })
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
