<div class="form-group">
    <label for="title">標題 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="title"
           :class="{'is-invalid': errors.title }"
           placeholder="標題" v-model="form.title"/>
    <span class="invalid-feedback" v-if="errors.title"
          role="alert"><strong>@{{ errors.title[0] }}</strong></span>
</div>
<div class="form-group">
    <label for="publish-at">發表日期 <span class="text-danger">*</span></label>
    <input type="date" class="form-control" id="publish-at"
           :class="{ 'is-invalid': errors.publish_at }"
           v-model="form.publish_at" placeholder="{{ now()->format('Y-m-d') }}">
    <span class="invalid-feedback" v-if="errors.publish_at"
          role="alert"><strong>@{{ errors.publish_at[0] }}</strong></span>
</div>
<div class="form-group">
    <div class="form-check form-check-flat form-check-primary">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" v-model.boolean="form.is_published"> 發表到學術活動 <i
                class="input-helper"></i>
        </label>
    </div>
</div>
<div class="form-group">
    <label for="content">內文</label>
    <summernote-textarea id="content" v-model="form.content" v-if="isLoaded"></summernote-textarea>
</div>
<div class="form-group">
    <label for="image">學術活動照片 </label>
    <div class="mb-2" v-if="form.image !== null">
        <img class="img-thumbnail" :src="'/storage/' + form.image" alt="最新消息圖片">
    </div>
    <input type="file" class="form-control" id="image" accept="image/*"
           :class="{ 'is-invalid': errors.image }"
           @change="uploadBanner">
    <span class="invalid-feedback" v-if="errors.image"
          role="alert"><strong>@{{ errors.image[0] }}</strong></span>
    <small id="imageHelpBlock" class="form-text text-muted">
        檔案上限為: {{ $max_upload_limit }} MB, 建議尺寸大小為 800x500 (比例16:10)
    </small>
</div>
