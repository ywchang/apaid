@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 編輯學術活動 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.events.index') }}">學術活動管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯學術活動</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                @include('admin.events.form')
                <div class="d-flex justify-content-between">
                    <div>
                        <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單</button>
                        <a class="btn btn-light" href="{{ route('admin.news.index') }}">返回列表</a>
                    </div>

                    <div>
                        <button data-toggle="modal" data-target="#exampleModal" class="btn btn-danger">刪除學術活動</button>
                    </div>
                </div>


                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">刪除學術活動</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                確定要刪除 @{{ form.title }} ？
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                                <button type="button" @click.prevent="deleteEvent" class="btn btn-inverse-danger">刪除
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <div v-else> 資料載入中...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                errors: {},
                tags: [],
                options: {},
                isLoaded: false
            },

            methods: {
                formSubmit() {
                    const vm = this;
                    axios.put('./', this.form)
                        .then(response => {
                            Swal.fire({
                                title: '更新成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.events.index') }}'
                                }
                            })
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '更新失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
                uploadBanner(e) {
                    const vm = this;
                    let file = e.target.files[0];

                    const form = new FormData;
                    form.append('file', file);
                    form.append('filepath', 'uploads/events');

                    axios.post('/admin/summernote/upload', form)
                        .then(xhr => vm.form.image = xhr.data.filePath)
                        .finally(() => e.target.value = '');
                },
                deleteEvent() {
                    axios.delete(`/admin/events/${this.form.id}`)
                        .then(() => {
                            Swal.fire({
                                title: '刪除成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.events.index') }}'
                                }
                            })
                        })
                }
            },
            mounted() {
                const vm = this;

                axios.get('./')
                    .then(res => {
                        vm.form = res.data.event;
                        vm.isLoaded = true
                    })
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
