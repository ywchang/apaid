@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 建立學術活動 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.events.index') }}">學術活動管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">建立學術活動</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <template v-if="isLoaded">
                @include('admin.events.form')
                <div>
                    <button type="submit" @click.prevent="formSubmit" class="btn btn-gradient-primary mr-2">送出表單
                    </button>
                    <a class="btn btn-light" href="{{ route('admin.news.index') }}">返回列表</a>
                </div>
            </template>
            <div v-else> 資料準備中...</div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        vueApp = {
            el: '#app',
            data: {
                form: {},
                errors: {},
                tags: [],
                options: {},
                isLoaded: false
            },

            methods: {
                formSubmit() {
                    const vm = this;
                    axios.post('./', this.form)
                        .then(response => {
                            Swal.fire({
                                title: '建立成功!',
                                text: '',
                                timer: 2000,
                                icon: 'success',
                                confirmButtonText: '確定',
                                onClose: () => {
                                    window.location = '{{ route('admin.events.index') }}'
                                }
                            })
                        })
                        .catch(error => {
                            Swal.fire({
                                title: '建立失敗!',
                                text: '',
                                timer: 2000,
                                icon: 'error',
                                confirmButtonText: '確定',
                            });

                            vm.errors = error.response.data.errors
                        })
                },
                uploadBanner(e) {
                    const vm = this;
                    let file = e.target.files[0];

                    const form = new FormData;
                    form.append('file', file);
                    form.append('filepath', 'uploads/events');

                    axios.post('/admin/summernote/upload', form)
                        .then(xhr => vm.form.image = xhr.data.filePath)
                        .finally(() => e.target.value = '');
                },
            },
            mounted() {
                const vm = this;

                axios.get('./new')
                    .then(res => {
                        vm.form = res.data.event
                        vm.isLoaded = true
                    })
            }
        };

        const app = new Vue(vueApp);
    </script>
@endsection
