@extends('layouts.admin')

@section('style')
    <style>
        tr td {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 350px;
        }
    </style>
@endsection


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 學術活動管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3"><a href="{{ route('admin.events.create') }}" class="btn btn-outline-linkedin">建立學術活動</a></div>
            <div>
                <table class="table table-hover mb-3">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>標題</th>
                        <th>發表日期</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($events as $key => $post)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->publish_at->format('Y-m-d') }}</td>
                            <td>
                                <a class="ml-1" href="{{ route('admin.events.edit', $post->id) }}"><i class="mdi mdi-lead-pencil menu-icon"></i> 編輯</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">還沒有學術活動哦！ <a href="{{ route('admin.events.create') }}">建立學術活動</a></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $events->links() }}
            </div>
        </div>
    </div>
@endsection
