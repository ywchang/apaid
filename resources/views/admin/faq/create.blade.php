@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 建立常見問題 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.faqs.index') }}">常見問題管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯常見問題</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.faqs.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="question">常見問題 <span class="text-danger">*</span></label>
                    <input type="text" id="question" name="question" placeholder="常見問題"
                           value="{{ old('question') }}"
                           class="form-control @error('question') is-invalid @enderror"/>
                    @error('question')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="answer">答覆內容</label>
                    <textarea class="form-control" placeholder="答覆內容 ..." id="answer" name="answer" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="enabled" value="1" checked> 啟用 <i
                                class="input-helper"></i>
                        </label>
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn btn-gradient-primary mr-2">送出表單</button>
                    <a class="btn btn-light" href="{{ route('admin.faqs.index') }}">返回列表</a>
                </div>
            </form>
        </div>
    </div>
@endsection
