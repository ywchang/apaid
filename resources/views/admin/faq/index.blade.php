@extends('layouts.admin')

@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 常見問題管理 </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
            </ul>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <div class="mb-3"><a href="{{ route('admin.faqs.create') }}" class="btn btn-outline-linkedin">新增常見問題</a>
            </div>
            <div class="table-responsive">
                <sortable-table
                    :list.sync="faqs"
                    v-on:update:list="handleSorting"
                >
                    <template v-slot:thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">問題</th>
                        <th scope="col">狀態</th>
                        <th scope="col">操作</th>
                    </tr>
                    </template>
                    <template v-slot:tbody>
                        <tr v-if="faqs.length === 0">
                            <td colspan="3">還沒有常見問題哦！ <a href="{{ route('admin.faqs.create') }}">開始新增常見問題</a></td>
                        </tr>
                        <tr v-else v-for="(item, index) in faqs" >
                            <td class="cursor-move"><i class="mdi mdi-cursor-move"></i></td>
                            <td v-text="item.question"></td>
                            <td v-text="item.enabled ? '啟用': '停用'"></td>
                            <td>
                                <a :href="`/admin/faqs/${item.id}/edit`">編輯</a>
                            </td>
                        </tr>
                    </template>
                </sortable-table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        new Vue({
            'el' : '#app',
            data: {
                faqs: []
            },
            methods: {
                handleSorting(items) {
                    axios.patch('/admin/faqs/sort', {
                        faqs: items.map(item => item.id)
                    });
                }
            },
            mounted() {
                this.faqs = @json($faqs)
            }
        })
    </script>
@endsection
