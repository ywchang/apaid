@extends('layouts.admin')


@section('content')
    {{-- page title --}}
    <div class="page-header">
        <h3 class="page-title"> 編輯常見問題 </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.faqs.index') }}">常見問題管理</a></li>
                <li class="breadcrumb-item active" aria-current="page">編輯常見問題</li>
            </ol>
        </nav>
    </div>
    {{-- content --}}
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.faqs.update', $faq->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="question">常見問題 <span class="text-danger">*</span></label>
                    <input type="text" id="question" name="question" placeholder="常見問題"
                           value="{{ old('question', $faq->question) }}"
                           class="form-control @error('question') is-invalid @enderror"/>
                    @error('question')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="answer">答覆內容</label>
                    <textarea class="form-control @error('answer') is-invalid @enderror" placeholder="答覆內容 ..." id="answer" name="answer" rows="5">{{ old('answer', $faq->answer) }}</textarea>
                    @error('answer')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="form-group">
                    <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="enabled" @if($faq->enabled) checked @endif> 啟用 <i
                                class="input-helper"></i>
                        </label>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <div>
                        <button type="submit" class="btn btn-gradient-primary mr-2">送出表單</button>
                        <a class="btn btn-light" href="{{ route('admin.faqs.index') }}">返回列表</a>
                    </div>
                    <div>
                        <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger">刪除常見問題</button>
                    </div>
                </div>
            </form>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">刪除常見問題</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            確定要刪除 {{ $faq->question }} ？
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                            <button type="button" onclick="deleteFaq()" class="btn btn-inverse-danger">刪除</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteFaq() {
            axios.delete("{{ route('admin.faqs.destroy', $faq->id) }}")
            .then(() => {
                Swal.fire({
                    title: '刪除成功!',
                    text: '',
                    timer: 2000,
                    icon: 'success',
                    confirmButtonText: '確定',
                    onClose: () => {
                        window.location = '{{ route('admin.faqs.index') }}'
                    }
                })
            })

        }
    </script>
@endsection
