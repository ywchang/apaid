require('./bootstrap');

// vendors
require('./admin/off-canvas');
require('./admin/hoverable-collapse');
require('./admin/misc');

window.Swal = require('sweetalert2');

window.Vue = require('vue');


Vue.component('summernote-textarea', require('./components/SummernoteTextarea.vue').default);
Vue.component('input-tag', require('vue-input-tag').default);
Vue.component('v-tags-input', require('@voerro/vue-tagsinput').default)
Vue.component('tags-sortable-table', require('./components/TagsSortableTable.vue').default)
Vue.component('sortable-table', require('./components/SortableTable.vue').default)
Vue.filter('prettyBytes', require('pretty-bytes'));

window.Summernote = {
    upload: function (file) {
        return new Promise((resolve, reject) => {
            const form = new FormData;
            form.append('file', file);
            form.append('filepath', 'summernote');

            const xhr = new XMLHttpRequest;
            xhr.open('POST', '/admin/summernote/upload');
            let token = document.head.querySelector('meta[name="csrf-token"]');

            if (token) {
                xhr.setRequestHeader('X-CSRF-Token', token.content);
                xhr.send(form);
            } else {
                console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
            }

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        xhr.responseJson = JSON.parse(xhr.response);
                        resolve(xhr);
                    } else {
                        reject(xhr);
                    }
                }
            }
        })
    }
};

(function (window) {
    window.VideoUploader = function (file) {
        const KB = 1024;
        const MB = 1024 * KB;
        const CHUNK_SIZE = 50 * MB;
        const UPLOAD_URL = '/admin/ajax/upload';

        if (!(file instanceof Blob)) {
            throw "not a Blob file";
        }

        this.file = file;
        this.chunk_index = 0;

        this.upload = function (resolve, reject) {
            let uploader = this;

            let start = CHUNK_SIZE * this.chunk_index++;
            let end = (start + CHUNK_SIZE) <= this.file.size
                ? start + CHUNK_SIZE
                : this.file.size;

            let form = new FormData;

            form.append('file', this.file.slice(start, end), this.file.name);

            let xhr = this.createXHR();

            xhr.onreadystatechange = function () {
                if (xhr.readyState === xhr.DONE) {
                    if (xhr.status === 200) {
                        if (uploader.chunk_index < Math.ceil(uploader.file.size / CHUNK_SIZE)) {
                            uploader.upload(resolve, reject);
                        } else {
                            xhr.responseJson = JSON.parse(xhr.response);
                            resolve(xhr);
                        }
                    } else {
                        reject(xhr)
                    }
                }
            };

            xhr.onprogress = function () {
                let percent = Math.round(start / uploader.file.size * 100) + "%";

                $('.progress-bar').css('width', percent);
                console.log("percent:" + percent)
            };

            xhr.onabort = function () {
                console.log(xhr);
            };

            xhr.send(form)
        }

        this.createXHR = function () {
            let xhr = new XMLHttpRequest;

            xhr.open('POST', UPLOAD_URL);

            xhr.setRequestHeader('X-CSRF-Token', $('[name=csrf-token]').attr('content'));

            return xhr
        }

    }

    VideoUploader.send = function (file) {
        let uploader = new VideoUploader(file);

        return new Promise((resolve, reject) => {
            uploader.upload(resolve, reject);
        })
    }
})(window);
