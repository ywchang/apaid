const { colors } = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './resources/**/*.vue',
        './resources/**/*.blade.php'
    ],
  theme: {
    extend: {},
    colors: colors,
    pagination: theme => ({
      color: theme('colors.green.500'),
      link: {
        'padding': '.25rem .75rem',
        'display': 'block',
        'text-decoration': 'none',
        'border-top-width': '1px',
        'border-left-width': '1px',
        'border-bottom-width': '1px',
        'background-color': '#ffffff',
      }
    })
  },
  variants: {},
  plugins: [
    require('tailwindcss-plugins/pagination'),
  ],
}
