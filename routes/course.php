<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('courses', 'CourseController@index')->name('home');

Route::get('courses/{course}', 'CourseController@show')->name('courses.show');

Route::get('api/courses/{course}', 'CourseController@api')->name('courses.api');

Route::get('courses/{course}/lessons/{lesson}', 'LessonController@show')->name('lessons.show');

Route::get('api/courses/{course}/lessons/{lesson}', 'LessonController@api')->name('lessons.api');

Route::get('material/{material}', 'MaterialController@show')->name('materials.show');

Route::post('courses/{course}/material/{material}/complete', 'MaterialController@complete')->name('materials.complete');

Route::get('profile', 'ProfileController@show')->name('profile.show');

Route::get('profile/records', 'ProfileController@records')->name('profile.records');

Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');

Route::post('profile/avatar', 'ProfileController@avatar')->name('profile.avatar');

Route::put('profile', 'ProfileController@update')->name('profile.update');

Auth::routes([
    'register' => false,
    'reset' => false,
    'confirm' => false,
    'verify' => false,
]);
