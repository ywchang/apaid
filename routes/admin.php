<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@redirect');
Route::get('docs', 'HomeController@docs')->name('docs');

Route::get('dashboard', 'HomeController@index')->name('home');

Auth::routes([
    'register' => false,
    'confirm' => false,
    'verify' => false
]);

Route::get('accounts', 'AccountController@show')->name('accounts.show');
Route::put('accounts', 'AccountController@update')->name('accounts.update');
Route::get('accounts/edit', 'AccountController@edit')->name('accounts.edit');

Route::get('members/export', 'MemberController@export')->name('members.export');
Route::resource('members', 'MemberController');

Route::post('tags/sort', 'TagController@sort');
Route::resource('tags', 'TagController');

Route::patch('faqs/sort', 'FaqController@sort');
Route::resource('faqs', 'FaqController');

Route::resource('news', 'NewsController');

Route::resource('events', 'EventController');

Route::resource('courses', 'CourseController');
Route::post('courses/{course}/members', 'CourseMemberController@store')->name('courses.members');
Route::get('courses/{course}/clone', 'CourseController@clone')->name('courses.clone');

Route::get('courses/{course}/learning-history', 'LearningHistoryController@index')->name('courses.learningHistory');
Route::get('courses/{course}/learning-history/export', 'LearningHistoryController@export')->name('courses.learningHistory.export');

Route::get('lessons', 'LessonController@index');
Route::delete('lessons/{lesson}', 'LessonController@destroy');

Route::name('courseLessons.')->group(function () {
    Route::get('courses/{course}/lessons', 'CourseLessonController@index')->name('index');
    Route::post('courses/{course}/lessons', 'CourseLessonController@store')->name('store');
    Route::get('courses/{course}/lessons/create', 'CourseLessonController@create')->name('create');
    Route::post('courses/{course}/lessons/add', 'CourseLessonController@add')->name('add');
    Route::get('courses/{course}/lessons/{lesson}', 'CourseLessonController@show')->name('show');
    Route::get('courses/{course}/lessons/{lesson}/edit', 'CourseLessonController@edit')->name('edit');
    Route::match(['put', 'patch'], 'courses/{course}/lessons/{lesson}', 'CourseLessonController@update')->name('update');
    Route::delete('courses/{course}/lessons/{lesson}', 'CourseLessonController@destroy')->name('destroy');
});

Route::get('materials', 'MaterialController@index')->name('materials.index');
Route::post('materials', 'MaterialController@store')->name('materials.store');
Route::put('materials/{material}', 'MaterialController@update')->name('materials.update');
Route::delete('materials/{material}', 'MaterialController@destroy')->name('materials.destroy');

// 關於我們
Route::get('about-us', 'AboutUsController@index')->name('about_us.index');
Route::get('about-us/{post}', 'AboutUsController@show')->name('about_us.show');
Route::get('about-us/{post}/edit', 'AboutUsController@edit')->name('about_us.edit');
Route::match(['put', 'patch'],'about-us/{post}', 'AboutUsController@update')->name('about_us.update');

// 申請流程管理
Route::get('applications', 'ApplicationController@index')->name('applications.index');
Route::get('applications/{application}', 'ApplicationController@show')->name('applications.show');
Route::get('applications/{application}/edit', 'ApplicationController@edit')->name('applications.edit');
Route::match(['put', 'patch'],'applications/{application}', 'ApplicationController@update')->name('applications.update');

// 頁面附件管理
Route::post('attachments', 'AttachmentController@store')->name('attachments.store');
Route::match(['put', 'patch'], 'attachments/{attachment}', 'AttachmentController@update')->name('attachment.update');
Route::delete('attachments/{attachment}', 'AttachmentController@destroy')->name('attachments.destroy');

// 聯絡我們管理
Route::get('contacts', 'ContactController@index')->name('contacts.index');
Route::get('contacts/export', 'ContactController@export')->name('contacts.export');
Route::get('contacts/{contact}', 'ContactController@show')->name('contacts.show');
Route::match(['put', 'patch'],'contacts/{contact}', 'ContactController@update')->name('contacts.update');

// 檔案上傳
Route::post('summernote/upload', 'SummernoteUploadController@store');
Route::post('ajax/upload', 'AjaxUploadController@store');

Route::get('variable-settings/{variableSetting}', 'VariableSettingController@get');
Route::post('variable-settings/{variableSetting}', 'VariableSettingController@set');
