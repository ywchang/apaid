<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

//關於
Route::get('history', 'AboutUsController@history')->name('about_us.history');
Route::get('structure', 'AboutUsController@structure')->name('about_us.structure');
Route::get('charter', 'AboutUsController@charter')->name('about_us.charter');

// 最新消息
Route::get('news', 'NewsController@index')->name('news.index');
Route::get('news/{news}', 'NewsController@show')->name('news.show');

// 學術活動
Route::get('events', 'EventController@index')->name('events.index');
Route::get('events/{event}', 'EventController@show')->name('events.show');

//
Route::get('doctors/search', 'DoctorsController@search')->name('doctors.search');

// 申請表頁面
Route::get('specialist-application', 'ApplicationController@specialist')->name('applications.specialist');
Route::get('fellowship-application', 'ApplicationController@fellowship')->name('applications.fellowship');
Route::get('general-application', 'ApplicationController@general')->name('applications.general');

Route::get('faqs', 'FaqController@index')->name('faqs');

Route::get('contact', 'ContactController@show')->name('contact');
Route::post('contact', 'ContactController@store')->name('contact');
