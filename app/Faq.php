<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['question', 'answer', 'enabled'];

    protected $casts = [
        'enabled' => 'boolean'
    ];

    public function scopeActive($query)
    {
        $query->where('enabled', true);
    }
}
