<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:191',
            'teacher' => 'nullable|max:191',
            'address' => 'nullable|max:191',
            'price' => 'numeric',
            'summary' => 'nullable',
            'banner' => 'nullable|max:191',
            'is_displayed' => 'boolean',
            'start_at' => 'required|date|date_format:Y-m-d',
            'end_at' => 'required|date|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '必須填寫課程名稱',
            'title.max' => '課程名稱不得超過191字',
            'teacher.max' => '教師名稱不得超過191字',
            'address.max' => '上課地點不得超過191字',
            'price.numeric' => '報名費用必須是數值',
            'start_at.required' => '必須填寫課程開始日',
            'start_at.*' => '課程開始日格式不正確',
            'end_at.required' => '必須填寫課程截止日',
            'end_at.*' => '課程截止日格式不正確',
        ];
    }
}
