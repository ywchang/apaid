<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'code' => ['required', 'max:191', Rule::unique('members', 'code')->ignore(request('id'))],
            'birth' => 'required|date',
            'gender' => 'required|in:1,2',
            'address' => 'required|max:191',
            'email.0' => 'required|max:50|email',
            'email.*' => 'max:50|email',
            'avatar' => 'nullable',
            'phone' => 'required|max:191',
            'mobile' => 'required|max:191',
            'line_id' => 'nullable|max:191',
            'facebook_name' => 'nullable|max:191',
            'identity' => 'required|max:191',
            'education' => 'nullable',
            'experience' => 'required',
            'clinic_name' => 'required|max:191',
            'clinic_city' => 'required',
            'clinic_district' => 'required|',
            'clinic_address' => 'required|max:191',
            'show_education' => 'boolean',
            'show_experience' => 'boolean',
            'show_current_job' => 'boolean',
            'certificate_number' => 'max:191',
            'payment_record' => 'array',
            'note' => 'nullable',
            'is_paid' => 'boolean',
            'is_checked' => 'boolean',
            'username' => ['required','max:191', Rule::unique('members', 'username')->ignore(request('id'))],
            'password' => 'sometimes|required|max:191',
            'role' => ['required', Rule::in(collect(config('apaid.role'))->keys())],
            'active' => 'boolean',
            'active_until' => 'nullable|date|date_format:Y-m-d',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '請填寫姓名',
            'code.required' => '請輸入會員代號',
            'code.unique' => '會員代號重覆',
            'birth.required' => '請輸入生日日期',
            'gender.required' => '請選擇性別',
            'email.0.required' => '至少輸入一組聯絡信箱',
            'address.required' => '請填寫聯絡地址請填寫',
            'phone.required' => '請填寫聯絡電話',
            'mobile.required' => '請填寫手機',
            'identity.required' => '請填寫身份',
            'experience.required' => '請填寫學 / 經歷資料',
            'clinic_name.required' => '請填寫診所名稱 / 職稱',
            'clinic_city.required' => '請填寫服務城市',
            'clinic_district.required' => '請填寫服務地區',
            'clinic_address.required' => '請填寫診所地址',
            'certificate_number.required' => '請填寫牙醫師證書編號',
            'username.required' => '必須輸入帳號',
            'password.required' => '必須輸入密碼',
            'role.required' => '必須選擇帳號角色',
        ];
    }
}
