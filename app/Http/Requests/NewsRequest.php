<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:191',
            'content' => 'nullable',
            'image' => 'required',
            'publish_at' => 'required|date|date_format:Y-m-d',
            'is_published' => 'boolean',
            'tags.0' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '請填寫標題',
            'title.max' => '標題不能超過191字',
            'image.required' => '必須上傳一張照片',
            'publish_at.required' => '請填寫發表日期',
            'publish_at.*' => '日期格式不正確',
            'tags.0.required' => '必須有一組分類'
        ];
    }
}
