<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:191',
            'code' => ['required', 'max:50' , Rule::unique('lessons', 'code')->ignore($this->get('id'))],
            'teacher' => 'required|max:191',
            'syllabus' => 'nullable',
            'start_at' => 'required|date|date_format:Y-m-d',
            'end_at' => 'required|date|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => '必須填寫課堂名稱',
            'title.max' => '課堂名稱不得超過191字',
            'code.required' => '必須填寫課堂代碼',
            'code.unique' => '課堂代碼已經存在',
            'code.max' => '課堂代碼不得超過50字',
            'teacher.required' => '必須填寫講師',
            'start_at.required' => '必須填寫開始時間',
            'start_at.*' => '開始時間格式不正確',
            'end_at.required' => '必須填寫結束時間',
            'end_at.*' => '結束時間格式不正確',
        ];
    }
}
