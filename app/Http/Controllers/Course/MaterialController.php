<?php

namespace App\Http\Controllers\Course;


use App\Course;
use App\Http\Controllers\Controller;
use App\Material;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\File;

class MaterialController extends Controller
{
    public function show(Material $material)
    {
        $file  = new File(storage_path('app/'. $material->path));

        return response()->file($file);
    }

    public function complete(Course $course, Material $material)
    {
        try {
            $this->authorize('view', [$material, $course]);

            auth()->user()->complete($material);
        } catch (AuthorizationException $exception) {
            return response()->json(['errors' => $exception->getMessage()], 403);
        }
    }
}
