<?php

namespace App\Http\Controllers\Course;

use App\Events\ProfileUpdated;
use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $member = auth()->user();
        $courses = $member->courses()->paginate(8);

        return view('course.profile-courses', compact('member', 'courses'));
    }

    public function records()
    {
        $member = auth()->user();
        return view('course.profile-records', compact('member'));
    }

    public function edit()
    {
        $member = auth()->user();
        return view('course.profile-edit', compact('member'));
    }

    public function update(Request $request)
    {
        $attributes = $this->validateRequest($request);

        if ($attributes['password']) {
            Arr::set($attributes, 'password', bcrypt($attributes['password']));
        } else {
            Arr::forget($attributes, 'password');
        }

        $attributes['show_education'] = $request->has('show_education');
        $attributes['show_experience'] = $request->has('show_experience');
        $attributes['show_current_job'] = $request->has('show_current_job');

        /** @var Member $member */
        $member = auth()->user();
        $member->update($attributes);

        event(new ProfileUpdated($member));

        return redirect()->route('course.profile.show');
    }

    public function avatar(Request $request)
    {
        $member = auth()->user();
        $path = sprintf('uploads/avatars/%s', now()->format('Ym'));
        $filePath = $request->file('file')->store($path, ['disk' => 'public']);
        $member->avatar = $filePath;
        $member->save();

        return response()->json(compact('filePath'));
    }

    public function validateRequest(Request $request)
    {
        return $this->validate($request, [
            'name' => 'required|max:191',
            'email.0' => 'required|email|max:191',
            'email.*' => 'nullable|email|max:191',
            'password' => 'nullable|max:191',
            'birth' => 'required|date',
            'gender' => 'in:1,2',
            'phone' => 'required|max:191',
            'mobile' => 'required|max:191',
            'facebook_name' => 'nullable',
            'line_id' => 'nullable',
            'education' => 'nullable',
            'experience' => 'required',
            'clinic_name' => 'required|max:191',
            'clinic_city' => 'required|max:191',
            'clinic_district' => 'required|max:191',
            'clinic_address' => 'required|max:191',
            'certificate_number' => 'max:191'
        ], [
            'name.required' => '請填寫姓名',
            'email.0.required' => '至少填寫一組電子郵件',
            'email.*.email' => '電子郵件的格式不正確',
            'birth.required' => '請填寫生日',
            'birth.date' => '日期格式不正確',
            'gender.in' => '性別不在選項中',
            'phone.required' => '請填寫聯絡電話',
            'mobile.required' => '請填寫手機',
            'facebook_name.max' => '長度不能超過191字',
            'line_id.max' => '長度不能超過191字',
            'experience.required' => '請填寫學 / 經歷資料',
            'clinic_name.required' => '請填寫診所名稱 / 職稱',
            'clinic_city.required' => '請填寫服務城市',
            'clinic_district.required' => '請填寫服務地區',
            'clinic_address.required' => '請填寫診所地址',
            'certificate_number.required' => '請填寫牙醫師證書編號',
        ]);
    }
}
