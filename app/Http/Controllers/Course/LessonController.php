<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\Http\Controllers\Controller;
use App\Lesson;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function show(Course $course, Lesson $lesson)
    {
        $this->authorize('view', [$lesson, $course]);

        return view('course.lesson', compact('course', 'lesson'));
    }

    public function api(Course $course, Lesson $lesson)
    {
        $this->authorize('view', [$lesson, $course]);

        $lesson->materials->each(function ($material) {
            $material->url = route('course.materials.show', $material->id);
        });

        $status = 'OK';

        return response()->json(compact('status', 'course', 'lesson'));
    }

}
