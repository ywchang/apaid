<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::visible()->orderByDesc('start_at')->paginate(12);

        return view('course.index', compact('courses'));
    }

    public function show(Course $course)
    {
        return view('course.show');
    }

    public function api(Course $course)
    {
        $member = auth()->user() ?? resolve(Member::class);

        $course->lessons->each(function ($lesson) use ($member, $course) {
            $lesson->canRead = $member->can('view', [$lesson, $course]);
        });

        return response()->json(compact('course'));
    }

}
