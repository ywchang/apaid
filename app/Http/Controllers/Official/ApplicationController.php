<?php

namespace App\Http\Controllers\Official;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * 專科研究員入會申請頁面
     */
    public function specialist()
    {
        $post = tap(Post::firstOrCreate(['post_name' => 'specialist_application']), function ($post) {
            $post->load('attachments');
        });

        return view('official.applications.specialist', compact('post'));
    }

    /**
     * 研究員入會申請頁面
     */
    public function fellowship()
    {
        $post = tap(Post::firstOrCreate(['post_name' => 'fellowship_application']), function ($post) {
            $post->load('attachments');
        });

        return view('official.applications.fellowship', compact('post'));
    }

    /**
     * 一般入會申請頁面
     */
    public function general()
    {
        $post = tap(Post::firstOrCreate(['post_name' => 'general_application']), function ($post) {
            $post->load('attachments');
        });

        return view('official.applications.general', compact('post'));

    }
}
