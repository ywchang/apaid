<?php

namespace App\Http\Controllers\Official;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $events = tap(Event::published(), function ($builder) {
            if ($year = request('year')) {
                $builder->whereYear('publish_at', '=', $year);
            }
        })->orderByDesc('publish_at')->paginate();

        $years = Event::selectRaw('YEAR(publish_at) year')
            ->groupByRaw('YEAR(publish_at)')
            ->orderByDesc('year')
            ->pluck('year');

        return view('official.events.index', compact('events', 'years'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $event
     * @return Application|Factory|Response|View
     */
    public function show(Event $event)
    {
        return view('official.events.show', compact('event'));
    }
}
