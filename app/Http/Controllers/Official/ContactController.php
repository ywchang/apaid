<?php

namespace App\Http\Controllers\Official;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Mail\MemberUpdated as MemberUpdatedMail;
use App\Mail\NewContactMail;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ContactController extends Controller
{
    public function show()
    {
        $tags = Tag::contact()->get();
        $option = compact('tags');
        return view('official.contact', compact('option'));
    }

    public function store(Request $request)
    {
        $this->againstRecaptcha();

        $attributes = $this->validateRequest($request);

        $contact = Contact::create($attributes);

        Mail::to(User::first())->send(new NewContactMail($contact));

        return redirect()->route('official.contact')->with('message', '謝謝您的提問與建議，我們會盡快聯繫您');
    }

    protected function againstRecaptcha()
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = [
            'secret' => config('services.recaptcha.secret'),
            'response' => request('recaptcha')
        ];
        $options = [
            'http' => [
                'header' => 'Content-type: application/x-www-form-urlencoded\r\n',
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        if ($resultJson->success === true) return true;

        logger('warning', ['resultJson: ', $resultJson]);
        throw ValidationException::withMessages(['recaptcha' => '表單已過期，請重新整理後再試一次。']);
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function validateRequest(Request $request): array
    {
        return $this->validate($request, [
            'name' => 'required|max:191',
            'reply_method' => 'in:1,2,3',              // @see config.apaid.contact.reply_method
            'gender' => 'nullable|in:1,2',             // @see config.apaid.gender
            'email' => 'required|max:191',
            'phone' => 'required|max:191',
            'type' => ['required', Rule::in(Tag::contact()->pluck('id'))],
            'content' => 'required',
        ], [
            'name.required' => '請輸入姓名',
            'name.max' => '姓名不能超過191字',
            'reply_method.in' => '所選希望聯繫方式不在選項中',
            'gender.in' => '所選性別不在選項中',
            'email.required' => '請輸入電子郵件',
            'email.max' => '電子郵件不能超過191字',
            'phone.required' => '請輸入聯絡電話',
            'type.required' => '請選擇問題類型',
            'type.in' => '問題類型不在選項裡',
            'content.required' => '請輸入問題內容'
        ]);
    }
}
