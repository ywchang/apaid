<?php

namespace App\Http\Controllers\Official;

use App\Http\Controllers\Controller;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DoctorsController extends Controller
{
    public function search()
    {
        $cities = Member::specialist()->get(['clinic_city', 'clinic_district'])
            ->groupBy(['clinic_city', 'clinic_district'])
            ->map(function ($city) { return $city->map->count(); })
            ->sortBy(function ($areas, $city) {
                return $city ? collect(config('apaid.city'))->flip()->get($city) : -1;
            });

        $members = tap(Member::specialist(), function ($builder) {
            if ($name = request('name')) { $builder->where('name', 'like',  "%$name%"); }
            if (request()->has('city')) { $builder->where('clinic_city', request('city')); }
            if (request('area')) { $builder->where('clinic_district', request('area')); }
        })->paginate(8);

        return view('official.doctors', compact('cities', 'members'));
    }
}
