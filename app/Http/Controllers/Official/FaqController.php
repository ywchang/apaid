<?php

namespace App\Http\Controllers\Official;

use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::active()
            ->orderBy('weight')
            ->paginate(8);

        return view('official.faqs.index', compact('faqs'));
    }
}
