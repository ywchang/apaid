<?php

namespace App\Http\Controllers\Official;

use App\Http\Controllers\Controller;
use App\Post;

class AboutUsController extends Controller
{
    protected $pages = [
        'history' => '學會沿革',
        'structure' => '組織架構',
        'charter' => '學會章程',
    ];

    public function history()
    {
        $action = 'history';
        $title = $this->pages[$action];
        $post = Post::firstOrCreate(['post_name' => $action]);
        return view('official.about-us', compact('post', 'title'));
    }

    public function structure()
    {
        $action = 'structure';
        $title = $this->pages[$action];
        $post = Post::firstOrCreate(['post_name' => $action]);
        return view('official.about-us', compact('post', 'title'));
    }

    public function charter()
    {
        $action = 'charter';
        $title = $this->pages[$action];
        $post = Post::firstOrCreate(['post_name' => $action]);
        return view('official.about-us', compact('post', 'title'));
    }
}
