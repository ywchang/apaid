<?php

namespace App\Http\Controllers\Official;

use App\Http\Controllers\Controller;
use App\News;
use App\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $builder = tap(News::published(), function ($builder) {
            $builder->whereHas('tags', function ($query) {
                $query->where('id', $this->withTag());
            });
        })->orderByDesc('publish_at');

        $news = $builder->paginate();
        $latest = $builder->first();

        $tags = Tag::news()->get();
        return view('official.news.index', compact('news', 'latest', 'tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\News $news
     * @return Application|Factory|Response|View
     */
    public function show(News $news)
    {
        return view('official.news.show', compact('news'));
    }

    /**
     * @return mixed
     */
    public function withTag()
    {
        return request()->get('tag', Tag::news()->first()->id ?? 1);
    }
}
