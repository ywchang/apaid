<?php

namespace App\Http\Controllers\Admin;

use App\Attachment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AttachmentController extends Controller
{
    public function store(Request $request)
    {
        try {
            $uploadedFile = $request->file('file');

            $filename = $uploadedFile->storeAs(
                'attachments',
                pathinfo($uploadedFile->hashName(), PATHINFO_FILENAME) . '.' . $uploadedFile->clientExtension(),
                ['disk' => 'public']);

            $attachment = Attachment::create([
                'path' => $filename,
                'name' => $uploadedFile->getClientOriginalName()
            ]);
        } catch (\Exception $exception) {
            throw ValidationException::withMessages([
                '上傳失敗'
            ]);
        }

        return response()->json(compact('attachment'));
    }

    public function update(Request $request, Attachment $attachment)
    {
        $attachment->update($request->only('name'));
        return response()->json('OK');
    }

    public function destroy(Attachment $attachment)
    {
        $attachment->delete();
        return response()->json('OK');
    }
}
