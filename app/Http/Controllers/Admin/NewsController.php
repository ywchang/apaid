<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\News;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $news = News::orderByDesc('publish_at')->paginate();
        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(NewsRequest $request)
    {
        $attributes = $request->validated();

        /** @var News $news */
        if ($news = News::create($attributes)) {
            $news->tags()->attach($request->get('tags'));
        }

        $status = 'OK';

        return response()->json(compact('status', 'news'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($news)
    {
        $news = News::findOrNew($news);

        $tags = Tag::news()->get()->map(function ($news) {
            return ['key' => $news->id, 'value' => $news->name];
        });

        $options = compact('tags');

        $tags = $news->tags->map(function ($tag) {
            return ['key' => $tag->id, 'value' => $tag->name];
        });

        return response()->json(compact('news', 'tags', 'options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(News $news)
    {
        return view('admin.news.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest $request
     * @param \App\News $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(NewsRequest $request, News $news)
    {
        $attributes = $request->validated();

        $news->update($attributes);

        $news->tags()->sync($request->get('tags'));

        return response()->json(['status' => 'OK', 'news' => $news]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\News $news
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(News $news)
    {
        $news->delete();

        return response()->json("OK");
    }
}
