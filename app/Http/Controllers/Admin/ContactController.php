<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Http\File;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::latest()->paginate();

        return view('admin.contacts.index', compact('contacts'));
    }

    public function show(Contact $contact)
    {
        return view('admin.contacts.show', compact('contact'));
    }

    public function update(Request $request, Contact $contact)
    {
        $contact->update([
            'note' => $request->get('note'),
            'is_checked' => $request->has('is_checked'),
        ]);

        return redirect()->route('admin.contacts.index');
    }

    public function export()
    {
        $writer = WriterEntityFactory::createXLSXWriter();

        $filename = sprintf("聯絡表單_%s.xlsx", now()->timestamp);
        $filepath = storage_path("app/tmp/$filename");
        $writer->openToFile($filepath);

        $cells = [
            WriterEntityFactory::createCell('問題類別'),
            WriterEntityFactory::createCell('姓名'),
            WriterEntityFactory::createCell('性別'),
            WriterEntityFactory::createCell('電子信箱'),
            WriterEntityFactory::createCell('電話'),
            WriterEntityFactory::createCell('希望回覆方式'),
            WriterEntityFactory::createCell('問題內容'),
            WriterEntityFactory::createCell('處理記錄'),
            WriterEntityFactory::createCell('狀態'),
            WriterEntityFactory::createCell('填表日期'),
        ];

        $singleRow = WriterEntityFactory::createRow($cells);
        $writer->addRow($singleRow);

        Contact::all()->each(function ($contact) use ($writer) {
            $rowFromValues = WriterEntityFactory::createRowFromArray([
                $contact->types->name,
                $contact->name,
                $this->formatGender($contact->gender),
                $contact->email,
                $contact->phone,
                $this->formatReplyMethod($contact->reply_method),
                $contact->content,
                $contact->note,
                $this->formatIsChecked($contact->is_checked),
                $contact->created_at->format("Y-m-d H:i:s")
            ]);

            $writer->addRow($rowFromValues);
        });

        $writer->close();

        return response()->file(new File($filepath))
            ->setContentDisposition('attachment', $filename);
    }

    /**
     * @param $gender
     * @return string
     */
    protected function formatGender($gender)
    {
        $lookup = config('apaid.gender');

        return (isset($lookup[$gender])) ? $lookup[$gender] : '';
    }

    /**
     * @param $isChecked
     * @return string
     */
    protected function formatIsChecked($isChecked)
    {
        return $isChecked ? '完成' : '未處理';
    }

    /**
     * @param $type
     * @return mixed
     */
    protected function formatReplyMethod($type)
    {
        $lookup = config('apaid.contact.reply_method');

        return isset($lookup[$type]) ? $lookup[$type] : '';
    }
}
