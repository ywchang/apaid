<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Requests\MemberRequest;
use App\Jobs\MemberExportJob;
use App\Member;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Arr;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $members = tap(Member::query(), function ($builder) {
            if ($keyword = request('keyword')) {
                $builder->where('name', 'like', "%$keyword%")
                    ->orWhere('code', 'like', "%$keyword%");
            }
            if ($role = request('role')) {
                $builder->where('role', $role);
            }
        })->paginate();
        return view('admin.members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Member $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Member $member)
    {
        return view('admin.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MemberRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MemberRequest $request)
    {
        $attributes = $request->validated();

        $attributes['password'] = bcrypt(Arr::get($attributes, 'password'));

        $member = Member::create($attributes);

        return response()->json(['status' => 'ok', 'member' => $member, 'redirect' => route('admin.members.index')]);
    }

    /**
     * Display the specified resource.
     *
     * @param Member $member
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($member)
    {
        $member = Member::findOrNew($member);

        return response()->json(compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Member $member
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Member $member)
    {
        return view('admin.members.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MemberRequest $request
     * @param Member $member
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(MemberRequest $request, Member $member)
    {
        $attributes = $request->validated();

        if ($request->get('password')) {
            $attributes['password'] = bcrypt(Arr::get($attributes, 'password'));
        }

        $member->update($attributes);

        return response()->json(['status' => 'OK', 'member' => $member]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Member $member
     * @return void
     */
    public function destroy(Member $member)
    {
        //
    }

    public function export()
    {
        return $this->dispatch(new MemberExportJob());
    }
}
