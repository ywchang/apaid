<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Requests\CourseRequest;
use App\Lesson;
use App\Member;
use App\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $courses = tap(Course::query(), function ($builder) {
            if ($keyword = request('keyword')) {
                $builder->where('title', 'like', "%$keyword%");
            }
        })->paginate();
        return view('admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CourseRequest $request
     * @return JsonResponse|RedirectResponse
     */
    public function store(CourseRequest $request)
    {
        $attributes = $request->validated();

        if ($course = Course::create($attributes)) {
            $course->tags()->sync($request->get('tags'));
        }

        if ($request->isXmlHttpRequest()) {
            return response()->json(['status' => 'OK', 'course' => $course]);
        }

        return redirect()->route('admin.courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param $course
     * @return JsonResponse|RedirectResponse
     */
    public function show($course)
    {
        $course = Course::findOrNew($course)->load(['members']);

        if (!request()->ajax()) {
            return redirect()->route('admin.courses.edit', $course->id);
        }

        $members = Member::get(['id', 'name', 'code']);
        $tags = Tag::course()->get()->map(function ($news) {
            return ['key' => $news->id, 'value' => $news->name];
        });
        $options = compact('members', 'tags');

        $tags = $course->tags->map(function ($tag) {
            return ['key' => $tag->id, 'value' => $tag->name];
        });

        return response()->json(compact('course', 'tags', 'options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @return Application|Factory|Response|View
     */
    public function edit(Course $course)
    {
        return view("admin.courses.edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CourseRequest $request
     * @param Course $course
     * @return JsonResponse|RedirectResponse
     */
    public function update(CourseRequest $request, Course $course)
    {
        $attributes = $request->validated();

        $course->update($attributes);

        $course->tags()->sync($request->get('tags'));

        $course->refresh()->load('members');

        if ($request->ajax()) {
            return response()->json(['status' => 'OK', 'course' => $course]);
        }

        return redirect()->route('admin.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Course $course)
    {
        $course->delete();

        $course->tags()->detach();
        $course->members()->detach();
        $course->lessons()->detach();

        return response()->json("OK");
    }

    public function clone(Course $course)
    {
        $replicateCourse = $course->replicate();

        $replicateLessons = $course->lessons->map(function ($lesson) {
            /** @var Lesson $lesson */
            $replicate = $lesson->replicate();
            $replicate->push();
            $replicate->materials()->sync($lesson->materials);
            return $replicate;
        });

        $replicateCourse->push();

        $replicateCourse->lessons()->saveMany($replicateLessons);

        $replicateCourse->tags()->sync($course->tags);

        return redirect()->route('admin.courses.edit', $replicateCourse->id);
    }
}
