<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AccountController extends Controller
{
    public function show()
    {
        $user = auth('admin')->user();

        return response()->json(compact('user'));
    }

    public function edit()
    {
        return view('admin.accounts.edit');
    }

    public function update(Request $request)
    {
        $attributes = $this->validate($request, [
            'email' => 'required|email|max:191',
            'password' => 'sometimes|nullable|max:191',
        ], [
            'email.not_regex' => '只允許一組 E-mail'
        ]);

        if (isset($attributes['password']) && !empty($attributes['password'])) {
            Arr::set($attributes, 'password', bcrypt($attributes['password']));
        } else {
            Arr::forget($attributes, 'password');
        }

        /** @var User $user */
        $user = auth('admin')->user();

        $user->update($attributes);

        $user->refresh();

        return response()->json(compact('user'));
    }
}
