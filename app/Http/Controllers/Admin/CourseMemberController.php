<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use Illuminate\Http\Request;

class CourseMemberController extends Controller
{
    public function store(Request $request, Course $course)
    {
        $this->validate($request, [
            'members.*.id' => 'exists:members,id'
        ], [
            'members.*.id.exists' => '學員不存在'
        ]);

        $members = $request->get('members');

        $course->members()->sync($members);

        return response()->json(['status' => 'ok', 'members' => $course->members]);
    }
}
