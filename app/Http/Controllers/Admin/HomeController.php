<?php

namespace App\Http\Controllers\Admin;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function redirect()
    {
        return redirect()->route('admin.home');
    }

    public function docs()
    {
        return redirect('https://ywchang.gitbook.io/apaid');
    }
}
