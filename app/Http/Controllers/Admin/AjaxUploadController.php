<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AjaxUploadController extends Controller
{
    /**
     * handle sliced upload file
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function store(Request $request)
    {
        if ($file = $request->file('file')) {
            $basename = md5(pathinfo($file->getClientOriginalName(), PATHINFO_BASENAME));
            $extension = $file->getClientOriginalExtension();

            $path = "app/tmp/$basename.$extension";
            $storage_path = storage_path($path);

            if (!File::isDirectory(storage_path('app/tmp'))) {
                File::makeDirectory(storage_path('app/tmp'), 0775, true, true);
            }

            File::append($storage_path, $file->get());

            return response()->json([
                'status' => 'ok',
                'file' => [
                    'origin_name' => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
                    'filename' => "$basename.$extension",
                    'basename' => File::name($storage_path),
                    'extension' => File::extension($storage_path),
                    'size' => File::size($storage_path),
                    'mime_type' => File::mimeType($storage_path),
                    'path' => $path
                ]
            ]);
        } else {
            response()->json([
                'status' => 'empty',
                'file' => [
                    'originalName' => '',
                    'filename' => '',
                    'basename' => '',
                    'extension' => '',
                    'size' => '',
                    'mimeType' => '',
                    'filepath' => '',
                ]
            ]);
        }
    }
}
