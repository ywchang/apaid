<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    protected $pages = [
        'history' => '學會沿革',
        'structure' => '組織架構',
        'charter' => '學會章程'
    ];

    public function index()
    {
        $posts = Post::whereIn('post_name', collect($this->pages)->keys())->get();
        $titles = $this->pages;
        return view('admin.about-us.index', compact('posts', 'titles'));
    }

    public function show(Post $post)
    {
        if (!request()->ajax()) {
            return redirect()->route('admin.about-us.edit', $post->id);
        }

        return response()->json(compact('post'));

    }

    public function edit(Post $post)
    {
        $title = $this->pages[$post->post_name];
        return view('admin.about-us.edit', compact('post', 'title'));
    }

    public function update(Request $request, Post $post)
    {
        $status = 'OK';

        $post->update($request->only('content'));

        return response()->json(compact('status', 'post'));
    }
}
