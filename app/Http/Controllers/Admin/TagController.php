<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Course;
use App\News;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $tags = Tag::query()->orderBy('weight')->get();
        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $attributes = $this->validate($request, [
            'name' => 'required|max:191',
            'type' => ['required', Rule::in([Course::class, News::class, Contact::class])]
        ], [
            'name.required' => '必須輸入分類標籤名稱',
            'name.max' => '分類標籤名稱不得超過191字',
            'type.required' => '必須選擇類別',
            'type.in' => '類別不在項目中'
        ]);

        Tag::create($attributes);

        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Tag $tag
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Tag $tag)
    {
        $attributes = $this->validate($request, [
            'name' => 'required|max:191',
        ], [
            'name.required' => '必須輸入分類標籤名稱',
            'name.max' => '分類標籤名稱不得超過191字',
        ]);

        $tag->update($attributes);

        return redirect()->route('admin.tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return response()->json('OK');
    }

    public function sort(Request $request)
    {
        DB::beginTransaction();
        collect($request->get('tags'))
            ->each(function ($tagId, $key) {
                Tag::query()
                    ->where('id', $tagId)
                    ->update(['weight' => $key]);
            });
        DB::commit();

        return response()->json('SORTED');
    }
}
