<?php

namespace App\Http\Controllers\Admin;

use App\Attachment;
use App\Post as Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    protected $applications = [
        'general_application' => '入會申請',
        'specialist_application' => '專科醫師申請',
        'fellowship_application' => '研究員申請'
    ];

    public function index()
    {
        $applications = Application::query()
            ->whereIn('post_name', collect($this->applications)->keys())
            ->get();

        $mapper = $this->applications;
        return view('admin.applications.index', compact('applications', 'mapper'));
    }

    public function show(Application $application)
    {
        if (!request()->ajax()) {
            return redirect()->route('admin.applications.edit', $application->id);
        }

        $application->load('attachments');
        return response()->json(compact('application'));

    }

    public function edit(Application $application)
    {
        $mapper = $this->applications;
        return view('admin.applications.edit', compact('application', 'mapper'));
    }

    public function update(Request $request, Application $application)
    {
        $status = 'OK';

        $application->update($request->only('content'));

        $application->attachments()->saveMany(
            Attachment::findMany(
                collect($request->get('attachments'))->pluck('id')
            )
        );

        $application->refresh()->load('attachments');

        return response()->json(compact('status', 'application'));
    }
}
