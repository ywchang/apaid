<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LessonController extends Controller
{
    public function index()
    {
        $lessons = tap(Lesson::paginate(), function ($paginator) {
            $paginator->each(function ($item) {
                $item->courses_count = $item->courses()->count();
            });
        });

        return response()->json(compact('lessons'));
    }

    public function destroy(Request $request, Lesson $lesson)
    {
        DB::transaction(function () use ($lesson) {
            $lesson->delete();
            $lesson->materials()->detach();
            $lesson->courses()->detach();
        });

        return response()->json("Material: " . $lesson->id . " Deleted");
    }
}
