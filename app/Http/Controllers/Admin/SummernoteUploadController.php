<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SummernoteUploadController extends Controller
{
    public function store(Request $request)
    {
        try {
            $file = $request->file('file');

            $filePath = $this->setSuffix($request->get('filepath'));

            $filePath = $file->hashName($filePath);

            Storage::disk('public')->put($filePath, $file->get());

            $response = [
                'status' => 'ok',
                'filePath' => $filePath,
                'message' => null
            ];
        } catch (\Exception $exception) {
            $status = 400;
            $response = [
                'status' => 'error',
                'filePath' => null,
                'message' => $exception->getMessage()
            ];

            Log::alert("[ Summerote 上傳失敗] {$exception->getMessage()}", request()->all());
        }

        return response()->json($response, $status ?? 200);
    }

    protected function setSuffix(string $filePath)
    {
        $suffix = now()->format('Ym');

        return "$filePath/$suffix";
    }
}
