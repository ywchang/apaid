<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Requests\LessonRequest;
use App\Lesson;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CourseLessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Course $course
     * @return Factory|View
     */
    public function index(Course $course)
    {
        $lessons = $course->lessons()->orderBy('start_at')->paginate();

        return view('admin.courseLessons.index', compact('course', 'lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Course $course
     * @param Lesson $lesson
     * @return Factory|View
     */
    public function create(Course $course, Lesson $lesson)
    {
        return view('admin.courseLessons.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LessonRequest $request
     * @param Course $course
     * @return JsonResponse
     */
    public function store(LessonRequest $request, Course $course)
    {
        $attributes = $request->validated();

        if ($lesson = $course->lessons()->create($attributes)) {
            $lesson->materials()->sync(
                collect($request->get('materials'))->mapWithKeys(function ($material, $key) {
                    return [$material['id'] => ['weight' => $key]];
                })
            );
        }

        return response()->json(['status' => 'OK', 'lesson' => $lesson]);
    }

    public function add(Request $request, Course $course)
    {
        $lessons = $request->get('lessons');

        $course->lessons()->attach($lessons);

        return redirect()->route('admin.courseLessons.index', $course);
    }

    /**
     * Display the specified resource.
     *
     * @param Course $course
     * @param Lesson $lesson
     * @return JsonResponse|RedirectResponse
     */
    public function show(Course $course, $lesson)
    {
        $lesson = Lesson::findOrNew($lesson)->load(['materials']);

        if (!request()->ajax()) {
            return redirect()->route('admin.courseLesson.edit', [$course->id, $lesson->id]);
        }

        return response()->json(compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @param Lesson $lesson
     * @return Factory|View
     */
    public function edit(Course $course, Lesson $lesson)
    {
        return view('admin.courseLessons.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LessonRequest $request
     * @param Course $course
     * @param Lesson $lesson
     * @return JsonResponse
     */
    public function update(LessonRequest $request, Course $course,  Lesson $lesson)
    {
        $attributes = $request->validated();

        try {
            $lesson->update($attributes);

            $lesson->materials()->sync(
                collect($request->get('materials'))->mapWithKeys(function ($material, $key) {
                    return [$material['id'] => ['weight' => $key]];
                })
            );
        } catch (\Exception $exception) {
            return response()->json(['status' => 'ERROR', 'errors' => $exception->getMessage()]);
        }

        $lesson->load('materials');

        return response()->json(['status' => 'OK', 'lesson' => $lesson->refresh()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @param Lesson $lesson
     * @return JsonResponse
     */
    public function destroy(Course $course, Lesson $lesson)
    {
        $lesson->courses()->detach();

        return response()->json('OK');
    }
}
