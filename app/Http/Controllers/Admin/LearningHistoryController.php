<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Material;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Http\File;
use Illuminate\Http\Request;

class LearningHistoryController extends Controller
{
    public function index(Course $course)
    {
        $course->load('lessons.materials.members');

        return view('admin.learningHistories.index', compact('course'));
    }

    public function export(Course $course)
    {
        $course->load('lessons.materials.members');

        $writer = WriterEntityFactory::createXLSXWriter();

        $filename = sprintf("%s_上課紀錄_%s.xlsx", $course->title, now()->timestamp);
        $filepath = storage_path("app/tmp/$filename");
        $writer->openToFile($filepath);

        $cells = [
            WriterEntityFactory::createCell('學員名稱'),
            WriterEntityFactory::createCell('學員代號'),
            WriterEntityFactory::createCell('課堂名稱'),
            WriterEntityFactory::createCell('課堂代號'),
            WriterEntityFactory::createCell('教材名稱'),
            WriterEntityFactory::createCell('完成時間'),
        ];

        $singleRow = WriterEntityFactory::createRow($cells);
        $writer->addRow($singleRow);

        $course->lessons->each(function ($lesson) use ($writer, $course) {
            $values = [$lesson->title, $lesson->code];
            $lesson->materials->each(function ($material) use ($writer, $values) {
                array_push($values, $material->name);

                $material->members->each(function ($member) use ($writer, $values) {
                    array_unshift($values, $member->name, $member->code);
                    array_push($values, $member->pivot->completed);

                    $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                    $writer->addRow($rowFromValues);
                });
            });
        });

        $writer->close();

        return response()->file(new File($filepath))
            ->setContentDisposition('attachment', $filename);
    }
}
