<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $faqs = Faq::query()->orderBy('weight')->get();
        return view('admin.faq.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $attributes = $this->validate($request, [
            'question' => 'required|max:191',
            'answer' => 'required',
        ], [
            'question.required' => '必須輸入常見問題',
            'question.max' => '問題字數不得超過191字',
            'answer.required' => '必須輸入答覆內容',
        ]);

        $attributes['enabled'] = $request->has('enabled');

        Faq::create($attributes);

        return redirect()->route('admin.faqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Faq $faq)
    {
        return redirect()->route('admin.faqs.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Faq $faq
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function edit(Faq $faq)
    {
        return view('admin.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Faq $faq
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Faq $faq)
    {
        $attributes = $this->validate($request, [
            'question' => 'required|max:191',
            'answer' => 'required',
        ], [
            'question.required' => '必須輸入常見問題',
            'question.max' => '問題字數不得超過191字',
            'answer.required' => '必須輸入答覆內容',
        ]);

        $attributes['enabled'] = $request->has('enabled');

        $faq->update($attributes);

        return redirect()->route('admin.faqs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Faq $faq
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();

        return response()->json("OK");
    }

    public function sort(Request $request)
    {
        DB::beginTransaction();
        collect($request->get('faqs'))
            ->each(function ($tagId, $key) {
                Faq::query()
                    ->where('id', $tagId)
                    ->update(['weight' => $key]);
            });
        DB::commit();

    }
}
