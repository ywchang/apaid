<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Material;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    public function index()
    {
        $materials = tap(Material::select(['id', 'name', 'type', 'path', 'size'])->paginate(), function ($paginator) {
            $paginator->each(function ($item) {
                $item->lessons_count = $item->lesson()->count();
            });
        });

        return response()->json(compact('materials'));
    }

    public function store(Request $request)
    {
        $file = new File(storage_path($request->get('path')), false);

        if (!$file->isFile()) {
            return response()->json('File does not exist', 400);
        }

        $material = Material::create([
            'name' => $request->get('name'),
            'type' => $this->getMaterialType($file),
            'mime_type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'path' => $request->get('path')
        ]);

        $material->refresh();

        return response()->json(compact('material'));
    }

    public function update(Request $request, Material $material)
    {
        $attributes = $this->validate($request, [
            'name' => 'nullable|max:191'
        ]);

        $material->update($attributes);

        $material->refresh();

        return response()->json(compact('material'));
    }

    public function destroy(Request $request, Material $material)
    {
        DB::transaction(function () use ($material) {
            $material->delete();
            $material->lesson()->detach();
        });

        return response()->json("Material: " . $material->id . " Deleted");
    }

    protected function getMaterialType(File $file)
    {
        if (preg_match('[pdf|video|image]', $file->getMimeType(), $match)) {
            return $match[0];
        }

        throw new \InvalidArgumentException('Invalid material type');
    }

}
