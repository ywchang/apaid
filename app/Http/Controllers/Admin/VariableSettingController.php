<?php

namespace App\Http\Controllers\Admin;

use App\VariableSetting;
use Illuminate\Http\Request;

class VariableSettingController extends Controller
{
    public function get(VariableSetting $variableSetting)
    {
        return response()->json($variableSetting->only(['name', 'value']));
    }

    public function set(Request $request, $name)
    {
        if (!$variableSetting = VariableSetting::where('name', $name)->first()) {
            $variableSetting = new VariableSetting(['name' => $name,]);
        }
        $variableSetting->value = $request->get('value');
        $variableSetting->save();

        return response()->json($variableSetting->only(['name', 'value']));
    }
}
