<?php

namespace App\Policies;

use App\Course;
use App\Lesson;
use App\Member;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    public function before(Member $member, $ability)
    {
        if ($member->isSuperMember()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the material.
     *
     * @param Member $member
     * @param Lesson $lesson
     * @param Course $course
     * @return mixed
     */
    public function view(Member $member, Lesson $lesson, Course $course)
    {
        return $course->members->contains($member)
            && $course->isDuring()
            && $lesson->isDuring();
    }
}
