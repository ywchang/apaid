<?php

namespace App\Policies;

use App\Course;
use App\Material;
use App\Member;
use Illuminate\Auth\Access\HandlesAuthorization;

class MaterialPolicy
{
    use HandlesAuthorization;

    public function before(Member $member, $ability)
    {
        if ($member->isSuperMember()) {
            return true;
        }
    }

    public function view(Member $member, Material $material, Course $course)
    {
        return $course->materials->contains('id', $material->id)
            && $course->members->contains($member);
    }

}
