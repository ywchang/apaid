<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['post_name', 'content'];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }
}
