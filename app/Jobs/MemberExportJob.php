<?php

namespace App\Jobs;

use App\Member;
use App\VariableSetting;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MemberExportJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function handle()
    {
        $writer = WriterEntityFactory::createXLSXWriter();

        $filename = $this->filename();
        $filepath = storage_path("app/tmp/$filename");
        $writer->openToFile($filepath);

        $title = ['會員編號', '姓名', '性別', '身份證字號', '生日', '電子信箱', '現任職務', '手機', '電話', '診所地址', '學/經歷', '牙醫師證書編號', 'Line ID', 'Facebook 名稱',];
        $variableSetting = VariableSetting::where('name', 'member_payment_title')->first() ?? [];

        $cells = collect($title)
            ->merge($variableSetting->value)
            ->map(function ($title) { return WriterEntityFactory::createCell($title); })
            ->toArray();

        $singleRow = WriterEntityFactory::createRow($cells);
        $writer->addRow($singleRow);

        Member::all()->each(function ($member) use ($writer, $variableSetting) {
            $value = [
                $member->code,
                $member->name,
                $this->formatGender($member->gender),
                $member->username,
                $this->formatBirthdate($member->birth),
                $this->formatEmail($member->email),
                $member->clinic_name,
                $member->mobile,
                $member->phone,
                $this->formatAddress($member),
                $member->experience,
                $member->certificate_number,
                $member->line_id,
                $member->facebook_name,
            ];

            collect($variableSetting->value)
                ->each(function($title) use ($member, &$value) {
                    $record = collect($member->payment_record)->where('title', $title)->first();
                    array_push($value, $record['amount'] ?? '');
                });


            $rowFromValues = WriterEntityFactory::createRowFromArray($value);

            $writer->addRow($rowFromValues);
        });

        $writer->close();

        return response()->file(new File($filepath))
            ->setContentDisposition('attachment', $filename);
    }

    /**
     * @param $gender
     * @return string
     */
    protected function formatGender($gender)
    {
        $lookup = config('apaid.gender');

        return (isset($lookup[$gender])) ? $lookup[$gender] : '';
    }

    protected function formatEmail($email)
    {
        return collect($email)->implode(PHP_EOL);
    }

    protected function formatBirthdate($birth)
    {
        return $birth ? sprintf('%s/%s/%s', $birth->subYears(1911)->year, $birth->month, $birth->day) : '';
    }

    protected function formatAddress($member)
    {
        return sprintf('%s%s%s',
            $member->clinic_city,
            $member->clinic_district,
            $member->clinic_address
        );
    }

    /**
     * @return string
     */
    public function filename(): string
    {
        return sprintf('會員清冊_%s.xlsx', now()->format('Ymd'));
    }
}
