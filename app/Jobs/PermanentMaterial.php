<?php

namespace App\Jobs;

use App\Material;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File as HttpFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PermanentMaterial
{
    use Dispatchable, SerializesModels;

    protected $material;

    /**
     * Create a new job instance.
     *
     * @param Material $material
     */
    public function __construct(Material $material)
    {
        $this->material = $material;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->permanentFile();

        $this->removeTmpUpload();
    }

    protected function permanentFile()
    {
        $material = Material::find($this->material->id);

        $material->path = Storage::putFile(
            "materials/" . now()->format('Ym'),
            new HttpFile(storage_path($material->path))
        );

        $material->save(['timestamp' => false]);
    }

    protected function removeTmpUpload()
    {
        File::delete(storage_path($this->material->path));
    }
}
