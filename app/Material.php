<?php

namespace App;

use App\Jobs\PermanentMaterial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Material extends Model
{
    protected $fillable = ['name', 'type', 'mime_type', 'size', 'path'];

    protected $appends = ['is_exist'];

    protected static function boot()
    {
        parent::boot();

        self::created(function ($material) {
            PermanentMaterial::dispatch($material);
        });

        self::deleted(function ($material) {
            Storage::delete($material->path);
        });
    }

    public function getIsExistAttribute()
    {
        return $this->isExist();
    }

    public function isExist()
    {
        return Storage::exists($this->path);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function lesson()
    {
        return $this->belongsToMany(Lesson::class, 'lesson_materials');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->lesson->first()->course();
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'member_materials')->withPivot('completed');
    }
}
