<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariableSetting extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'value'];

    protected $casts = [
        'value' => 'array'
    ];

    public function getRouteKeyName()
    {
        return 'name';
    }
}
