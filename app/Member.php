<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class Member extends Authenticatable
{
    protected static function boot()
    {
        parent::boot();

        self::updating(function (Member $member) {
            if ($member->isDirty('avatar')) {
                $member->removeOldAvatar();
            }
        });

        self::deleting(function (Member $member) {
            $member->removeOldAvatar();
        });
    }

    protected $fillable = [
        'name', 'code', 'birth', 'gender', 'role', 'address', 'email', 'avatar', 'phone', 'mobile', 'identity', 'education', 'experience', 'clinic_name', 'clinic_city', 'clinic_district', 'clinic_address', 'show_education', 'show_experience', 'show_current_job', 'certificate_number', 'payment_record', 'note',
        'line_id', 'facebook_name',
        'is_paid', 'is_checked',
        'username', 'password', 'active', 'active_until'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $casts = [
        'email' => 'array',
        'birth' => 'date:Y-m-d',
        'payment_record' => 'array',
        'show_education' => 'boolean',
        'show_experience' => 'boolean',
        'show_current_job' => 'boolean',
        'is_paid' => 'boolean',
        'is_checked' => 'boolean',
        'active' => 'boolean',
        'active_until' => 'date:Y-m-d',
    ];

    protected $attributes = [
        'name' => null,
        'code' => null,
        'birth' => null,
        'gender' => null,
        'role' => null,
        'address' => null,
        'email' => '[]',
        'avatar' => null,
        'phone' => null,
        'mobile' => null,
        'line_id' => null,
        'facebook_name' => null,
        'identity' => null,
        'education' => null,
        'experience' => null,
        'clinic_name' => null,
        'clinic_city' => null,
        'clinic_district' => null,
        'clinic_address' => null,
        'show_education' => TRUE,
        'show_experience' => TRUE,
        'show_current_job' => TRUE,
        'certificate_number' => null,
        'payment_record' => '[]',
        'note' => null,
        'is_paid' => FALSE,
        'is_checked' => FALSE,

        'username' => null,
        'password' => null,
        'active' => TRUE,
        'active_until' => null
    ];

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_members');
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class, 'member_materials')->withPivot('completed');
    }

    public function isSuperMember()
    {
        return $this->role === 'superuser';
    }

    public function role()
    {
        return config('apaid.role')[$this->role];
    }

    protected function removeOldAvatar()
    {
        Storage::disk('public')->delete($this->getOriginal('avatar'));
    }
    /**
     * @param Material $material
     * @return array
     */
    public function complete(Material $material)
    {
        return $this->materials()->syncWithoutDetaching([
            $material->id => ['completed' => now()]
        ]);
    }

    public function isComplete(Material $material)
    {
        return $material->members->contains($this);
    }

    public function scopeSpecialist($query)
    {
        $query->where('role', 'specialist');
    }

    public function scopeTemporary($query)
    {
        $query->where('role', 'temporary');
    }
}
