<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::updating(function (News $news) {
            if ($news->isDirty('image')) {
                Storage::disk('public')->delete($news->getOriginal('image'));
            }
        });

        self::deleted(function ($news) {
            $news->tags()->sync([]);
            Storage::disk('public')->delete($news->image);
        });
    }

    protected $fillable = ['title', 'content', 'image', 'publish_at', 'is_published'];

    protected $attributes = [
        'title' => null,
        'content' => null,
        'image' => null,
        'publish_at' => null,
        'is_published' => true
    ];

    protected $casts = [
        'publish_at' => 'datetime:Y-m-d',
        'is_published' => 'boolean'
    ];

    public function scopePublished($query)
    {
        $query->whereDate('publish_at', '<=', now())
            ->where('is_published', 1);
    }

    public function isPublished(): bool
    {
        return now()->isAfter($this->publish_at) && $this->is_published;
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
