<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    protected $fillable = ['path', 'name'];

    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $attachment) {
            Storage::disk('public')->delete($attachment->path);
        });
    }
}
