<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewContactMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.NewContact', ['contact' => $this->contact])
            ->replyTo($this->contact->email, $this->contact->name)
            ->subject('來自【台灣亞洲植牙醫學會】的聯絡表單');
    }
}
