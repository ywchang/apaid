<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'type'];

    public function scopeCourse($query)
    {
        $query->where('type', Course::class);
    }

    public function scopeNews($query)
    {
        $query->where('type', News::class);
    }

    public function scopeContact($query)
    {
        $query->where('type', Contact::class);
    }
}
