<?php

namespace App\Listeners;

use App\Events\ProfileUpdated;
use App\Mail\MemberUpdated as MemberUpdatedMail;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class MemberUpdated
{
    /**
     * Handle the event.
     *
     * @param  ProfileUpdated  $event
     * @return void
     */
    public function handle(ProfileUpdated $event)
    {
        $member = $event->member;

        if ($event->wasChanged()) {
            $diff = $member->getChanges();

            Mail::to($this->admin())
                ->send(new MemberUpdatedMail($member));
        }
    }

    /**
     * @return User
     */
    public function admin()
    {
        return User::first();
    }
}
