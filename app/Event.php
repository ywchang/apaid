<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Event extends Model
{
    protected $fillable = ['title', 'content', 'image', 'publish_at', 'is_published'];

    protected $attributes = [
        'title' => null,
        'content' => null,
        'image' => null,
        'publish_at' => null,
        'is_published' => true
    ];

    protected $casts = [
        'publish_at' => 'datetime:Y-m-d',
        'is_published' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::updating(function ($event) {
            if ($event->isDirty('image')) {
                Storage::disk('public')->delete($event->getOriginal('image'));
            }
        });

        self::deleted(function ($event) {
            Storage::disk('public')->delete($event->image);
        });
    }

    public function scopePublished($query)
    {
        $query->whereDate('publish_at', '<=', now())
            ->where('is_published', 1);
    }

    public function isPublished(): bool
    {
        return now()->isAfter($this->publish_at) && $this->is_published;
    }
}
