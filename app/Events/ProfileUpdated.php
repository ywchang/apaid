<?php

namespace App\Events;

use App\Member;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProfileUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $attentionAttributes = ['name', 'birth', 'gender', 'address', 'email', 'phone', 'mobile', 'education', 'experience', 'clinic_name', 'clinic_city', 'clinic_district', 'clinic_address', 'certificate_number', 'line_id', 'facebook_name'];
    /**
     * @var bool
     */
    protected $wasChanged;
    /**
     * @var Member
     */
    public $member;

    /**
     * Create a new event instance.
     *
     * @param Member $member
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
        $this->wasChanged = $member->wasChanged($this->attentionAttributes);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function wasChanged()
    {
        return $this->wasChanged;
    }
}
