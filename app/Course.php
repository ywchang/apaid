<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Course extends Model
{
    protected $fillable = ['title', 'teacher', 'address', 'price', 'summary', 'banner', 'start_at', 'end_at', 'is_displayed'];

    protected $dates = ['start_at', 'end_at'];

    protected $casts = [
        'start_at' => 'date:Y-m-d',
        'end_at' => 'date:Y-m-d',
        'is_displayed' => 'boolean'
    ];

    protected $attributes = [
        'title' => '',
        'teacher' => null,
        'address' => null,
        'price' => 0,
        'summary' => '',
        'banner' => null,
        'is_displayed' => true,
        'start_at' => null,
        'end_at' => null,
    ];

    protected static function boot()
    {
        parent::boot();

        static::updating(function (Course $course) {
            if ($course->isDirty('banner')) {
                Storage::disk('public')->delete($course->getOriginal('banner'));
            }
        });
    }

    public function getMaterialsAttribute()
    {
        return $this->materials();
    }

    public function materials()
    {
        return $this->lessons->pluck('materials')->flatten();
    }

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class, 'course_lessons');
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'course_members');
    }

    public function addMember(Member $member)
    {
        $this->members()->attach($member);
    }

    /**
     * @param $lesson
     * @return false|Model
     */
    public function addLesson(Lesson $lesson)
    {
        return $this->lessons()->save($lesson);
    }

    public function scopeVisible($query)
    {
        $query->where('is_displayed', true);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function isDuring()
    {
        return now()->between($this->start_at, $this->end_at);
    }
}
