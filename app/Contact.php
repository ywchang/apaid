<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['type', 'name', 'gender', 'email', 'phone', 'reply_method', 'content', 'note', 'is_checked'];

    protected $casts = [
        'is_checked' => 'boolean'
    ];

    public function types()
    {
        return $this->belongsTo(Tag::class, 'type');
    }
}
