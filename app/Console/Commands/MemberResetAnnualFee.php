<?php

namespace App\Console\Commands;

use App\Member;
use Illuminate\Console\Command;

class MemberResetAnnualFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apaid:member:reset-annual-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '常年會費繳費重新計算';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Member::query()->update(['is_paid' => false]);
    }
}
