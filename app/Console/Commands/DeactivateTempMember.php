<?php

namespace App\Console\Commands;

use App\Member;
use Illuminate\Console\Command;

class DeactivateTempMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apaid:member:deactivate-temp-member';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '停用過期的暫時性會員';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Member::temporary()
            ->whereDate('active_until', '<', now())
            ->update(['active' => false]);
    }
}
