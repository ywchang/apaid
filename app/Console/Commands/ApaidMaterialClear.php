<?php

namespace App\Console\Commands;

use App\Material;
use Illuminate\Console\Command;

class ApaidMaterialClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apaid:material:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'remove all materials without course id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $materials = Material::doesntHave('lesson')->get();

        $materials->each->delete();

        $this->info('Delete ' . $materials->count() . ' materials');
    }
}
