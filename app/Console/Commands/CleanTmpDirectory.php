<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CleanTmpDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apaid:tmp:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean files from app/tmp directory';

    protected $deleted = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        collect(glob(storage_path('app/tmp/*')))
            ->each(function ($realpath) {
                $file = new \Illuminate\Http\File($realpath);
                $lastAccessTime = Carbon::createFromTimestamp($file->getATime());
                if (now()->diffInHours($lastAccessTime) > 23) {
                    $this->delete($realpath);
                }
            });

        $this->info(sprintf('清理了 %s 個檔案', collect($this->deleted)->count()));
    }

    private function delete($realpath)
    {
        File::delete($realpath);
        $this->deleted[] = $realpath;
    }
}
