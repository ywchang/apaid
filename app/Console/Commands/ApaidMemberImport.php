<?php

namespace App\Console\Commands;

use App\Member;
use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\XLSX\Sheet;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ApaidMemberImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apaid:member:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '一次性的資料匯入';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open(storage_path('apaid.xlsx'));

        $members = collect();
        /** @var Sheet $sheet */
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            /** @var Row $row */
            foreach ($sheet->getRowIterator() as $key => $row) {
                // do stuff with the row

                if ($key == 1) continue;

                /** @var Cell[] $cell */
                $cell = $row->getCells();
                $member = new Member([
                    'name' => $cell[4]->getValue(),
                    'code' => $cell[2]->getValue(),
                    'birth' => $this->formatBirth($cell[7]->getValue()),
                    'gender' => $this->formatGender($cell[5]->getValue()),
                    'role' => $this->formatRole($cell[3]->getValue()),
                    'address' => $cell[12]->getValue(),
                    'email' => $this->formatEmail($cell[8]->getValue()),
                    'phone' => $cell[10]->getValue(),
                    'mobile' => $cell[10]->getValue(),
                    'identity' => $this->formatIdentity($cell[1]->getValue()),
                    'education' => $cell[13]->getValue(),
                    'experience' => $cell[13]->getValue(),
                    'clinic_name' => $cell[9]->getValue(),
                    'clinic_address' => $cell[12]->getValue(),
                    'certificate_number' => null,
                    'username' => $cell[6]->getValue(),
                    'password' => bcrypt($this->formatPassword($cell[7]->getValue())),
                ]);

                $members->add($member);
            }
        }

        $members->each(function ($member) {
            try {
                if (empty($member->code)) {
                    throw new \Exception('Code Empty');
                }
                if (empty($member->username)) {
                    throw new \Exception('Username Empty');
                }
                $member->save();
            } catch (\Exception $exception) {

                Log::debug('import fail: ' . $exception->getMessage(), [$member]);
            }
        });
    }

    protected function formatGender($value)
    {
        $map = ['男' => 1, '女' => 2];
        return $map[$value] ?? null;
    }

    protected function formatEmail($value)
    {
        return explode(',', $value);
    }

    protected function formatBirth($value)
    {
        if (!$value instanceof \DateTime)
            return null;
        return $value->format('Y-m-d');
    }

    protected function formatRole($value)
    {
        if (str_contains($value, '專科醫師')) {
            return 'specialist';
        } else if (str_contains($value, '研究員')) {
            return 'fellowship';
        } else {
            return 'general';
        }
    }

    protected function formatPassword($value)
    {
        if (!$value instanceof \DateTime)
            return '123456';

        return sprintf('%s%s%s',
            (int)$value->format('Y') - 1911,
            $value->format('m'),
            $value->format('d')
        );
    }

    protected function formatCode($value)
    {
        if ($value) {
            return $value;
        }

        throw new \Exception('code empty');
    }

    protected function formatIdentity($value)
    {
        return !empty($value) ? $value : '會員';
    }
}
