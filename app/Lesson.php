<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['title', 'code', 'teacher', 'syllabus', 'start_at', 'end_at'];

    protected $dates = ['start_at', 'end_at'];

    protected $casts = [
        'start_at' => 'date:Y-m-d',
        'end_at' => 'date:Y-m-d'
    ];

    protected $attributes = [
        'title' => '',
        'code' => '',
        'teacher' => null,
        'syllabus' => null,
        'start_at' => null,
        'end_at' => null
    ];

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_lessons');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function materials()
    {
        return $this->belongsToMany(Material::class, 'lesson_materials')->withPivot('weight')->orderBy('weight');
    }

    public function isDuring()
    {
        return now()->between($this->start_at, $this->end_at);
    }
}
